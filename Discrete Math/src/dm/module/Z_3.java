
package dm.module;

import dm.util.*;

/**
 *
 * @author Kravchenko Alexey
 */ 
class Z_3 {
        public static Z MUL_ZM_Z(Z oldZ)
    {
        return Z.create (oldZ.abs (), !oldZ.sign());
    }
}
