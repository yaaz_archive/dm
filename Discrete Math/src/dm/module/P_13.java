package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class P_13 {
	
	
	public static P NMR_P_P(P p) {
		P r=Procedures.DIV_PP_P(p, Procedures.GCF_PP_P(p, Procedures.DER_P_P(p)));
		PBuilder b=PBuilder.create();
		for(int i=0;i<=r.degree();i++) b.setCoefficient(i, Procedures.RED_Q_Q(r.getCoefficient(i)));
		r=b.get();
		Q fac=Procedures.FAC_P_Q(r);
		if(!r.getCoefficient(r.degree()).sign()) fac=Q.create(Procedures.MUL_ZM_Z(fac.numerator()), fac.denominator());
		for(int i=0;i<=r.degree();i++) b.setCoefficient(i, Procedures.RED_Q_Q(Procedures.DIV_QQ_Q(r.getCoefficient(i), fac)));
		return b.get();
	}
	
	
}
