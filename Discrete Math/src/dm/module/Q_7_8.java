package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class Q_7_8 {
	
	
	public static Q MUL_QQ_Q(Q a, Q b) {
		return Procedures.RED_Q_Q(Q.create(Procedures.MUL_ZZ_Z(a.numerator(), b.numerator()), Procedures.MUL_NN_N(a.denominator(), b.denominator())));
	}
	
	
	public static Q DIV_QQ_Q(Q a, Q b) {
		Z n=Procedures.MUL_ZZ_Z(a.numerator(), b.denominator());
		Z d=Procedures.MUL_ZZ_Z(a.denominator(), b.numerator());
		if(!d.sign()) n=Procedures.MUL_ZM_Z(n);
		return Procedures.RED_Q_Q(Q.create(n, d.abs()));
	}
	
	
}
