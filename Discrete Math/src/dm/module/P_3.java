package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class P_3 {
	
	
	public static P MUL_PQ_P(P p, Q q) {
		PBuilder r=PBuilder.create();
		for(int i=0;i<=p.degree();i++) r.setCoefficient(i, Procedures.MUL_QQ_Q(p.getCoefficient(i), q));
		return r.get();
	}
	
	
}
