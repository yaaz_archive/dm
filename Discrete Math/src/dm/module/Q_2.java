package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class Q_2 {
	
	
	public static boolean INT_Q_B(Q q) {
		return Procedures.MOD_ZZ_Z(q.numerator(), q.denominator()).isZero();
	}
	
	
}
