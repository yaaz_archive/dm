
package dm.module;

import dm.util.*;
/**
 *
 * @author Kravchenko Alexey
 */ 
class Z_2 {
            public static byte POZ_Z_D(Z mainZ)
    {
        if (mainZ.isZero())
        {
            return 0;
        }
        if(mainZ.sign())
        {
            return 2;
        }
        if(!mainZ.sign())
        {
            return 1;
        }
                return 0;
    }
}
