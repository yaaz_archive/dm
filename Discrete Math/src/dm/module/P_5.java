package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class P_5 {
	
	
	public static Q LED_P_Q(P p) {
		return p.getCoefficient(p.degree());
	}
	
	
}
