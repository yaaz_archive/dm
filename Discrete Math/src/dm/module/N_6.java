package dm.module; 
 
import dm.util.N; 
import dm.util.NBuilder; 
 
 
/** 
 * @author Angelina Kuzmina 
 **/ 
 
class N_6 { 
 
 public static N MUL_ND_N(N n, int d) { 
 NBuilder b = NBuilder.create(); 
 int rest = 0; 
 int tmp = 0; 
 
 for (int i = 0; i <= n.getDigitsNumber(); i++) { 
 tmp = (n.getDigit(i) * d); 
 
 if (tmp > 9 && i != n.getDigitsNumber()) { 
 b.setDigit(i, (tmp + rest) % 10); 
 rest = (int) ((double) (tmp + rest) / 10); 
 } else { 
 tmp = tmp + rest; 
 if (tmp > 9 && i != n.getDigitsNumber()) { 
 rest = (int) ((double) (tmp) / 10); 
 b.setDigit(i, tmp % 10); 
 } else { 
 b.setDigit(i, tmp); 
 rest = 0; 
 } 
 } 
 } 
 return b.get(); 
 } 
}