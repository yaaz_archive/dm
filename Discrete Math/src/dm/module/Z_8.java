package dm.module;
import dm.util.*;
/**
 *
 * @author Kravchenko Alexey
 */
class Z_8 
{
    public static Z MUL_ZZ_Z(Z a, Z b)
    {
        int k=0;
        if(Procedures.POZ_Z_D(a)!=Procedures.POZ_Z_D(b))
        {
            k=1;
        }
        N n=Procedures.MUL_NN_N(Procedures.ABS_Z_N(a),Procedures.ABS_Z_N(b));
        if(k==1)
        {
            return Z.create(n, false);
        }
        else
        {
            return Z.create(n, true);
        }
    }
}