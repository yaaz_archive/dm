package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class Q_3_4 {
	
	
	public static Q TRANS_Z_Q(Z z) {
		return z;
	}
	
	
	public static Z TRANS_Q_Z(Q q) {
		return (Z) q;
	}
	
	
}
