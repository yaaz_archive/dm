package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class P_6 {
	
	
	public static N DEG_P_N(P p) {
		return N.create(p.degree());
	}
	
	
}
