package dm.module;
import dm.util.*;

/**
 *
 * @author Kravchenko Alexey
 */
class N_4 
{
    public static N ADD_NN_N(N a, N b)
    {
        int max;
        if (Procedures.COM_NN_D(a,b)==2)
        {
            max=a.getDigitsNumber();
        }
        else
        {
            max=b.getDigitsNumber();
        }
        NBuilder c = NBuilder.create();
        for(int i =0; i<max; i++)
        {
           int s=a.getDigit(i) + b.getDigit(i) + c.getDigit (i);
            if (s>9) c.setDigit (i+1, 1);
            c.setDigit (i, s%10);
        }
        return c.get();
    }
}