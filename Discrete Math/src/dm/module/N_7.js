//Author: Kirill Kovalenko
//Port to js: Nikita Gubarkov
N_7={
		MUL_Nk_N:function(n, k) {
			if(k==0) return n;
	        var b=NBuilder.create();
	        for(var i=n.getDigitsNumber()+k;i>=k;i--) {
	        	var d=n.getDigit(i-k);
	            b.setDigit(i, d);
	        }
	        for(var i=k-1;i>=0;i--) b.setDigit(i, 0);
	        return b.get();
		}
};