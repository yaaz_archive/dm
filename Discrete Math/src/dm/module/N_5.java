package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class N_5 {
	
	
	public static N SUB_NN_N(N a, N b) {
		NBuilder n=NBuilder.create();
		int lastnz=-1;
		for(int i=a.getDigitsNumber()-1;i>=0;i--) {
			int d=a.getDigit(i)-b.getDigit(i);
			if(d<0) {
				d+=10;
				for(int j=i+1;j<lastnz;j++) n.setDigit(j, 9);
				n.setDigit(lastnz, n.getDigit(lastnz)-1);
			}
			n.setDigit(i, d);
			if(d!=0) lastnz=i;
		}
		return n.get();
	}
	
	
}
