package dm.module;

import dm.util.*;


public class Procedures {
	
	
	/**
	 * Compare natural number {@code a} and {@code b}
	 * @return 2 if {@code a>b}, 1 if {@code a<b} and 0 if {@code a=b}
	 */
	public static int COM_NN_D(N a, N b) {
		return N_1.COM_NN_D(a, b);
	}
	
	
	/**
	 * Check if natural number is zero
	 * @return true if {@code n} is zero, false otherwise
	 */
	public static boolean NZER_N_B(N n) {
		return (boolean) Js.eval("N_2.NZER_N_B(ARG0)", n);
	}
	
	
	/**
	 * Add 1 to natural number
	 * @return n+1
	 */
	public static N ADD_1N_N(N n) {
		return (N) Js.eval("N_3.ADD_1N_N(ARG0)", n);
	}
	
	
	/**
	 * Add 2 natural numbers
	 * @return a+b
	 */
	public static N ADD_NN_N(N a, N b) {
		return N_4.ADD_NN_N(a, b);
	}
	
	
	/**
	 * Subtract natural numbers (only if a is greater than b)
	 * @return a-b
	 */
	public static N SUB_NN_N(N a, N b) {
		if(Procedures.COM_NN_D(a, b)==1) throw new IllegalArgumentException("b is bigger than a");
		return N_5.SUB_NN_N(a, b);
	}
	
	
	/**
	 * Multiply natural number and digit
	 * @return n*d
	 */
	public static N MUL_ND_N(N n, int d) {
		if(d<0||d>9) throw new IllegalArgumentException("d should be between 0 and 9");
		return N_6.MUL_ND_N(n, d);
	}
	
	
	/**
	 * Multiply natural number and power of 10
	 * @return n*10^k
	 */
	public static N MUL_Nk_N(N n, long k) {
		if(k<0) throw new IllegalArgumentException("k should be 0 or greater");
		return (N) Js.eval("N_7.MUL_Nk_N(ARG0, "+k+")", n);
	}
	
	
	/**
	 * Multiply natural numbers
	 * @return a*b
	 */
	public static N MUL_NN_N(N a, N b) {
		return (N) Js.eval("N_8.MUL_NN_N(ARG0, ARG1)", a, b);
	}
	
	
	/**
	 * Subtract natural number multiplied by digit from natural number (only if result is non-negative)
	 * @return a-b*d
	 */
	public static N SUB_NDN_N(N a, int d, N b) {
		return N_9.SUB_NDN_N(a, d, b);
	}
	
	
	/**
	 * Compute first digit of natural number's division multiplied by 10^k where k is that digit's position counting from 0 (only if a>=b & b!=0)
	 * @return firstDigit(a/b)*10^firstDigitPosition(a/b)
	 */
	public static Dk DIV_NN_Dk(N a, N b) {
		if(b.isZero()) throw new IllegalArgumentException("b should be greater than 0");
		return N_10.DIV_NN_Dk(a, b);
	}
	
	
	/**
	 * Divide natural numbers (only if a>=b & b!=0)
	 * @return a/b
	 */
	public static N DIV_NN_N(N a, N b) {
		if(b.isZero()) throw new IllegalArgumentException("b should be greater than 0");
		return N_11.DIV_NN_N(a, b);
	}
	
	
	/**
	 * Mod of 2 natural number's division (only if a>=b & b!=0)
	 * @return a%b
	 */
	public static N MOD_NN_N(N a, N b) {
		if(b.isZero()) throw new IllegalArgumentException("b should be greater than 0");
		return (N) Js.eval("N_12.MOD_NN_N(ARG0, ARG1)", a, b);
	}
	
	
	/**
	 * Gcf of 2 natural numbers
	 * @return max k where {@code a⋮k & b⋮k}
	 */
	public static N GCF_NN_N(N a, N b) {
		if(b.isZero()||a.isZero()) throw new IllegalArgumentException("a and b should be greater than 0");
		return (N) Js.eval("N_13.GCF_NN_N(ARG0, ARG1)", a, b);
	}
	
	
	/**
	 * Lcm of 2 natural numbers
	 * @return min k where {@code k⋮a & k⋮b}
	 */
	public static N LCM_NN_N(N a, N b) {
		if(b.isZero()||a.isZero()) throw new IllegalArgumentException("a and b should be greater than 0");
		return (N) Js.eval("N_14.LCM_NN_N(ARG0, ARG1)", a, b);
	}
	
	
	
	
	
	/**
	 * Integer absolute value
	 * @return abs(z)
	 */
	public static N ABS_Z_N(Z z) {
		return Z_1.ABS_Z_N(z);
	}
	
	
	/**
	 * Sign of an integer
	 * @return 2 if {@code z>0}, 0 if {@code z=0} and 1 if {@code z<0}
	 */
	public static int POZ_Z_D(Z z) {
		return Z_2.POZ_Z_D(z);
	}
	
	
	/**
	 * Multiply integer by -1
	 * @return -z
	 */
	public static Z MUL_ZM_Z(Z z) {
		return Z_3.MUL_ZM_Z(z);
	}
	
	
	/**
	 * Convert natural number to integer
	 * @return (Z) n
	 */
	public static Z TRANS_N_Z(N n) {
		return Z_4.TRANS_N_Z(n);
	}
	
	
	/**
	 * Convert positive integer to natural number
	 * @return (N) z
	 */
	public static N TRANS_Z_N(Z z) {
		if(!z.sign()) throw new IllegalArgumentException("z should be positive");
		return Z_5.TRANS_Z_N(z);
	}
	
	
	/**
	 * Add 2 integers
	 * @return a+b
	 */
	public static Z ADD_ZZ_Z(Z a, Z b) {
		return (Z) Js.eval("Z_6.ADD_ZZ_Z(ARG0, ARG1)", a, b);
	}
	
	
	/**
	 * Subtract 2 integers
	 * @return a-b
	 */
	public static Z SUB_ZZ_Z(Z a, Z b) {
		return (Z) Js.eval("Z_7.SUB_ZZ_Z(ARG0, ARG1)", a, b);
	}
	
	
	/**
	 * Multiply 2 integers
	 * @return a*b
	 */
	public static Z MUL_ZZ_Z(Z a, Z b) {
		return Z_8.MUL_ZZ_Z(a, b);
	}
	
	
	/**
	 * Divide 2 integers (only if b!=0)
	 * @return a/b
	 */
	public static Z DIV_ZZ_Z(Z a, Z b) {
		if(b.isZero()) throw new IllegalArgumentException("b cannot be 0");
		return Z_9.DIV_ZZ_Z(a, b);
	}
	
	
	/**
	 * Mod of 2 integer's division (only if b!=0)
	 * @return a%b
	 */
	public static Z MOD_ZZ_Z(Z a, Z b) {
		if(b.isZero()) throw new IllegalArgumentException("b cannot be 0");
		return Z_10.MOD_ZZ_Z(a, b);
	}
	
	
	
	
	
	/**
	 * Reduce a fraction
	 * @return reduce(q)
	 */
	public static Q RED_Q_Q(Q q) {
		return Q_1.RED_Q_Q(q);
	}
	
	
	/**
	 * Check if fraction is integer
	 * @return isInteger(q)
	 */
	public static boolean INT_Q_B(Q q) {
		return Q_2.INT_Q_B(q);
	}
	
	
	/**
	 * Convert integer to fraction
	 * @return (Q) z
	 */
	public static Q TRANS_Z_Q(Z z) {
		return Q_3_4.TRANS_Z_Q(z);
	}
	
	
	/**
	 * Convert fraction to integer (only if denominator is 1)
	 * @return (Z) q
	 */
	public static Z TRANS_Q_Z(Q q) {
		if(!q.denominator().isOne()) throw new IllegalArgumentException("denominator should be 1");
		return Q_3_4.TRANS_Q_Z(q);
	}
	
	
	/**
	 * Add 2 fractions
	 * @return a+b
	 */
	public static Q ADD_QQ_Q(Q a, Q b) {
		return Q_5_6.ADD_QQ_Q(a, b);
	}
	
	
	/**
	 * Subtract 2 fractions
	 * @return a-b
	 */
	public static Q SUB_QQ_Q(Q a, Q b) {
		return Q_5_6.SUB_QQ_Q(a, b);
	}
	
	
	/**
	 * Multiply 2 fractions
	 * @return a*b
	 */
	public static Q MUL_QQ_Q(Q a, Q b) {
		return Q_7_8.MUL_QQ_Q(a, b);
	}
	
	
	/**
	 * Divide 2 fractions
	 * @return a/b
	 */
	public static Q DIV_QQ_Q(Q a, Q b) {
		if(b.isZero()) throw new IllegalArgumentException("b cannot be 0");
		return Q_7_8.DIV_QQ_Q(a, b);
	}
	
	
	
	
	
	/**
	 * Add 2 polynomials
	 * @return a+b
	 */
	public static P ADD_PP_P(P a, P b) {
		return P_1.ADD_PP_P(a, b);
	}
	
	
	/**
	 * Subtract 2 polynomials
	 * @return a-b
	 */
	public static P SUB_PP_P(P a, P b) {
		return P_2.SUB_PP_P(a, b);
	}
	
	
	/**
	 * Multiply polynomial by fraction
	 * @return p*q
	 */
	public static P MUL_PQ_P(P p, Q q) {
		return P_3.MUL_PQ_P(p, q);
	}
	
	
	/**
	 * Multiply polynomial by x^k
	 * @return p*x^k
	 */
	public static P MUL_Pxk_P(P p, int k) {
		if(k<0) throw new IllegalArgumentException("k should be 0 or greater");
		return P_4.MUL_Pxk_P(p, k);
	}
	
	
	/**
	 * Leading coefficient of a polynomial
	 * @return led(p)
	 */
	public static Q LED_P_Q(P p) {
		return P_5.LED_P_Q(p);
	}
	
	
	/**
	 * Degree of a polynomial
	 * @return deg(p)
	 */
	public static N DEG_P_N(P p) {
		return P_6.DEG_P_N(p);
	}
	
	
	/**
	 * Gcf of numerators and lcm of denominators of a polynomial's coefficients
	 * @return fac(p)
	 */
	public static Q FAC_P_Q(P p) {
		return P_7.FAC_P_Q(p);
	}
	
	
	/**
	 * Multiply 2 polynomials
	 * @return a*b
	 */
	public static P MUL_PP_P(P a, P b) {
		return P_8.MUL_PP_P(a, b);
	}
	
	
	/**
	 * Divide 2 polynomials
	 * @return a/b
	 */
	public static P DIV_PP_P(P a, P b) {
		if(b.isZero()) throw new IllegalArgumentException("b cannot be 0");
		return P_9_10.DIV_PP_P(a, b);
	}
	
	
	/**
	 * Mod of 2 polynomial's division
	 * @return a%b
	 */
	public static P MOD_PP_P(P a, P b) {
		if(b.isZero()) throw new IllegalArgumentException("b cannot be 0");
		return P_9_10.MOD_PP_P(a, b);
	}
	
	
	/**
	 * Gcf of 2 polynomials
	 * @return max k where {@code a⋮k & b⋮k}
	 */
	public static P GCF_PP_P(P a, P b) {
		if(a.isZero()||b.isZero()) throw new IllegalArgumentException("a and b cannot be 0");
		return P_11.GCF_PP_P(a, b);
	}
	
	
	/**
	 * Derivative of polynomial
	 * @return der(p)
	 */
	public static P DER_P_P(P p) {
		return P_12.DER_P_P(p);
	}
	
	
	/**
	 * Convert to polynomial with prime roots
	 * @return nmr(p)
	 */
	public static P NMR_P_P(P p) {
		return P_13.NMR_P_P(p);
	}
	
	
}
