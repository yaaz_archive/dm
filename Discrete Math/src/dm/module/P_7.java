package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class P_7 {
	
	
	public static Q FAC_P_Q(P p) {
		N num=p.getCoefficient(0).numerator().abs(), den=p.getCoefficient(0).denominator();
		for(int i=1;i<=p.degree();i++) {
			Z n=p.getCoefficient(i).numerator();
			if(!n.isZero()) {
				num=Procedures.GCF_NN_N(num, n.abs());
				den=Procedures.LCM_NN_N(den, p.getCoefficient(i).denominator());
			}
		}
		return Q.create(num, den);
	}
	
	
}
