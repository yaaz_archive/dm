package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class P_9_10 {
	
	
	public static P DIV_PP_P(P a, P b) {
		return divide(a, b, false);
	}
	
	
	
	public static P MOD_PP_P(P a, P b) {
		return divide(a, b, true);
	}
	
	
	
	private static P divide(P a, P b, boolean mod) {
		PBuilder dv=null;
		if(!mod) dv=PBuilder.create();
		while(a.degree()>=b.degree()) {
			Q q=Procedures.DIV_QQ_Q(a.getCoefficient(a.degree()), b.getCoefficient(b.degree()));
			P t=Procedures.MUL_PQ_P(b, q);
			t=Procedures.MUL_Pxk_P(t, a.degree()-b.degree());
			if(!mod) dv.setCoefficient(a.degree()-b.degree(), q);
			a=Procedures.SUB_PP_P(a, t);
		}
		return mod?a:dv.get();
	}
	
	
}
