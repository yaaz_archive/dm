package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class N_10 {
	
	
	public static Dk DIV_NN_Dk(N a, N b) {
		int k=a.getDigitsNumber()-b.getDigitsNumber();
		if(k<0) return Dk.create(0, 0);
		if(k==0) {
			int c=Procedures.COM_NN_D(a, b);
			if(c==0) return Dk.create(1, 0);
			if(c==1) return Dk.create(0, 0);
		}
		int d=9;
		for(;;) {
			N r=Procedures.MUL_Nk_N(Procedures.MUL_ND_N(b, d), k);
			int div=Procedures.COM_NN_D(a, r);
			if(div!=1) return Dk.create(d, k);
			d--;
			if(d==0) {
				d=9;
				k--;
			}
		}
	}
	
	
}
