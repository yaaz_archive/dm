package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class P_11 {
	
	
	public static P GCF_PP_P(P a, P b) {
		if(a.isZero()) return b;
		if(b.isZero()) return a;
		if(Procedures.DIV_PP_P(a, b).isZero()) {
			P t=a;
			a=b;
			b=t;
		}
		return gcf(a, b);
	}
	
	
	
	private static P gcf(P a, P b) {
		a=Procedures.MOD_PP_P(a, b);
		if(a.isZero()) return b;
		return gcf(b, a);
	}
	
	
}
