//Author: Nikita Gubarkov
N_14={
		LCM_NN_N:function(n1, n2) {
			return Procedures.DIV_NN_N(Procedures.MUL_NN_N(n1, n2), Procedures.GCF_NN_N(n1, n2));
		}
};