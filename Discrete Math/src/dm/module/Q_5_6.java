package dm.module;

import java.util.function.BiFunction;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class Q_5_6 {
	
	
	public static Q ADD_QQ_Q(Q a, Q b) {
		return as(a, b, Procedures::ADD_ZZ_Z);
	}
	
	
	public static Q SUB_QQ_Q(Q a, Q b) {
		return as(a, b, Procedures::SUB_ZZ_Z);
	}
	
	
	private static Q as(Q a, Q b, BiFunction<Z, Z, Z> func) {
		N lcm=Procedures.LCM_NN_N(a.denominator(), b.denominator());
		Z an=Procedures.MUL_ZZ_Z(a.numerator(), Procedures.DIV_NN_N(lcm, a.denominator()));
		Z bn=Procedures.MUL_ZZ_Z(b.numerator(), Procedures.DIV_NN_N(lcm, b.denominator()));
		return Procedures.RED_Q_Q(Q.create(func.apply(an, bn), lcm));
	}
	
	
}
