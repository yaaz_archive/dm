package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class N_11 {
	
	
	public static N DIV_NN_N(N a, N b) {
		NBuilder n=NBuilder.create();
		while(!a.isZero()) {
			Dk t=Procedures.DIV_NN_Dk(a, b);
			if(t.getDigit()==0) break;
			a=Procedures.SUB_NDN_N(a, t.getDigit(), Procedures.MUL_Nk_N(b, t.getDigitsNumber()-1));
			n.setDigit(t.getDigitsNumber()-1, t.getDigit());
		}
		return n.get();
	}
	
	
}
