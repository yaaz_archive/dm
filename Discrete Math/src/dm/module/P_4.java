package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class P_4 {
	
	
	public static P MUL_Pxk_P(P p, int k) {
		if(k==0) return p;
		PBuilder b=PBuilder.create();
		for(int i=0;i<=p.degree();i++) b.setCoefficient(i+k, p.getCoefficient(i));
		return b.get();
	}
	
	
}
