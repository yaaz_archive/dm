package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class P_8 {
	
	
	public static P MUL_PP_P(P a, P b) {
		P res=N.ZERO;
		for(int i=0;i<=b.degree();i++) {
			P p=Procedures.MUL_Pxk_P(Procedures.MUL_PQ_P(a, b.getCoefficient(i)), i);
			res=Procedures.ADD_PP_P(res, p);
		}
		return res;
	}
	
	
}
