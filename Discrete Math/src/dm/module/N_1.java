package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class N_1 {
	
	
	public static int COM_NN_D(N a, N b) {
		int la=a.getDigitsNumber(), lb=b.getDigitsNumber();
		if(la!=lb) return la>lb?2:1;
		for(int i=la-1;i>=0;i--) {
			byte da=a.getDigit(i), db=b.getDigit(i);
			if(da!=db) return da>db?2:1;
		}
		return 0;
	}
	
	
}
