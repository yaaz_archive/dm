package dm.module;

import java.io.InputStreamReader;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import dm.Main;


class Js {
	
	
	private static final ScriptEngine engine;
	static {
		engine=new ScriptEngineManager().getEngineByName("nashorn");
		eval("Procedures=Java.type('dm.module.Procedures')");
		eval("N=Java.type('dm.util.N')");
		eval("Z=Java.type('dm.util.Z')");
		eval("Q=Java.type('dm.util.Q')");
		eval("P=Java.type('dm.util.P')");
		eval("NBuilder=Java.type('dm.util.NBuilder')");
		eval("PBuilder=Java.type('dm.util.PBuilder')");
		load('N', 2, 3, 7, 8, 12, 13, 14);
		load('Z', 6, 7);
	}
	protected static void init() {}
	protected static Object eval(String s) {
		try {
			return engine.eval(s);
		} catch (ScriptException e) {
			throw new RuntimeException(e);
		}
	}
	protected static Object eval(String s, Object arg0) {
		engine.put("ARG0", arg0);
		return eval(s);
	}
	protected static Object eval(String s, Object arg0, Object arg1) {
		engine.put("ARG1", arg1);
		return eval(s, arg0);
	}
	private static void load(char lit, int... nums) {
		try {
			for(int n : nums) engine.eval(new InputStreamReader(Main.class.getResourceAsStream("/dm/module/"+lit+"_"+n+".js")));
		} catch(ScriptException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	
}
