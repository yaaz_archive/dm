//Author: Anastasia Sharikova
Z_6={
		ADD_ZZ_Z:function(a, b) {
			var res=N.ZERO;
			var m=Procedures.POZ_Z_D(a);
			if(m==2){
				var n=Procedures.POZ_Z_D(b);
				if(n==2){
					res=Procedures.ADD_NN_N(a,b);
					return res;
				}
				else if(n==1) {
					k = Procedures.ABS_Z_N(b);
					var l = Procedures.COM_NN_D(a,k);
					if (l==2){
					res = Procedures.SUB_NN_N(a,k);
					return res;
					}
					else {
							var sum = Procedures.SUB_NN_N(k,a);
							res = Procedures.MUL_ZM_Z(sum);
							return res;
						}
					}
				else return a;
				}
			else if (m==1) {
				n=Procedures.POZ_Z_D(b);
				if(n==1){
					k=Procedures.ABS_Z_N(a);
					var s = Procedures.ABS_Z_N(b);
					sum = Procedures.ADD_NN_N(k,s);
					res = Procedures.MUL_ZM_Z(sum);
					return res;
				}
				else if(n==2) {
					k=Procedures.ABS_Z_N(a);
					l=Procedures.COM_NN_D(k,b);
					if(l==2){
						sum =Procedures.SUB_NN_N(k,b);
						res=Procedures.MUL_ZM_Z(sum);
						return res;
					}
					else{
						res = Procedures.SUB_NN_N(b,k);
						return res;
					}
				}
				else return a;

			}
			else return b;
		}
};