//Author: Nikita Gubarkov
N_8={
		MUL_NN_N:function(a, b) {
			N_8.mulDigitArr=[];
			var res=N.ZERO;
			for(var i=0;i<a.getDigitsNumber();i++) {
				var n=N_8.getMulDigit(b, a.getDigit(i));
				n=Procedures.MUL_Nk_N(n, i);
				res=Procedures.ADD_NN_N(res, n);
			}
			return res;
		},
		mulDigitArr:null,
		getMulDigit:function(n, d) {
			var r=N_8.mulDigitArr[d];
			if(r!=undefined) return r;
			r=Procedures.MUL_ND_N(n, d);
			N_8.mulDigitArr[d]=r;
			return r;
		}
};