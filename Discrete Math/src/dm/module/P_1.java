package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class P_1 {
	
	
	public static P ADD_PP_P(P a, P b) {
		int deg=Math.max(a.degree(), b.degree());
		PBuilder r=PBuilder.create();
		for(int i=0;i<=deg;i++) r.setCoefficient(i, Procedures.ADD_QQ_Q(a.getCoefficient(i), b.getCoefficient(i)));
		return r.get();
	}
	
	
}
