package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class Q_1 {
	
	
	public static Q RED_Q_Q(Q q) {
		if(q.isZero()) return N.ZERO;
		N gcf=Procedures.GCF_NN_N(q.numerator().abs(), q.denominator());
		return Q.create(Procedures.DIV_ZZ_Z(q.numerator(), gcf), Procedures.DIV_NN_N(q.denominator(), gcf));
	}
	
	
}
