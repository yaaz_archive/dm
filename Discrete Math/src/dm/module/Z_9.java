package dm.module;
import dm.util.*;
/**
 *
 * @author Kravchenko Alexey
 */
class Z_9 
{ 
    public static Z DIV_ZZ_Z(Z a, Z b)
    {
        boolean s=true;
        if(Procedures.POZ_Z_D(a) != Procedures.POZ_Z_D(b)) s=false;
        N n=Procedures.DIV_NN_N(Procedures.ABS_Z_N(a),Procedures.ABS_Z_N(b));
        return Z.create(n, s);
    }
}