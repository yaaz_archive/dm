//Author: Kirill Kovalenko
//Port to js: Nikita Gubarkov
N_13={
		GCF_NN_N:function(n1, n2) {
			var c=n1;
			while(!c.isZero()) {
				c = Procedures.MOD_NN_N(n1, n2);
				n1 = n2;
				n2 = c;
			}
			return n1;
		}
};