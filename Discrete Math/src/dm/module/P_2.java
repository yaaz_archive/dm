package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class P_2 {
	
	
	public static P SUB_PP_P(P a, P b) {
		int deg=Math.max(a.degree(), b.degree());
		PBuilder r=PBuilder.create();
		for(int i=0;i<=deg;i++) r.setCoefficient(i, Procedures.SUB_QQ_Q(a.getCoefficient(i), b.getCoefficient(i)));
		return r.get();
	}
	
	
}
