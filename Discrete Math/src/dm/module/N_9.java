package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class N_9 {
	
	
	public static N SUB_NDN_N(N a, int d, N b) {
		N t=Procedures.MUL_ND_N(b, d);
		if(Procedures.COM_NN_D(a, t)==1) throw new IllegalArgumentException("b*d is bigger than a");
		return Procedures.SUB_NN_N(a, t);
	}
	
	
}
