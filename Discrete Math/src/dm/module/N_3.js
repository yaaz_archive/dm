//Author: Nikita Gubarkov
N_3={
		ADD_1N_N:function(n) {
			var b=NBuilder.create();
	        var off=1;
	        for(var i=0;i<n.getDigitsNumber();i++) {
	        	var d=n.getDigit(i);
	            b.setDigit(i, (d+off)%10);
	            if(d!=9) off=0;
	        }
	        if(off==1) b.setDigit(n.getDigitsNumber(), 1);
	        return b.get();
		}
};