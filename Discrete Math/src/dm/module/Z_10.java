package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class Z_10 {
	
	
	public static Z MOD_ZZ_Z(Z a, Z b) {
		return Procedures.SUB_ZZ_Z(a, Procedures.MUL_ZZ_Z(Procedures.DIV_ZZ_Z(a, b), b));
	}
	
	
}
