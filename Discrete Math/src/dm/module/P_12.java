package dm.module;

import dm.util.*;


/**
 * @author Nikita Gubarkov
 */
class P_12 {
	
	
	public static P DER_P_P(P p) {
		PBuilder b=PBuilder.create();
		for(int i=1;i<=p.degree();i++) b.setCoefficient(i-1, Procedures.MUL_QQ_Q(p.getCoefficient(i), N.create(i)));
		return b.get();
	}
	
	
}
