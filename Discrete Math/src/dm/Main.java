package dm;

import dm.gui.GUI;


/**
* @author Nikita Gubarkov
*/
public class Main {
	
	
	public static void main(String[] args) {
		System.setProperty("sun.awt.noerasebackground", "true");
		GUI.initMock();
		GUI.init();
	}
	
	
	
	public static void exit() {
		System.exit(0);
	}
	
	
}
