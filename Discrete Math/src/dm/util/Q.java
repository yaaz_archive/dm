package dm.util;


/**
* @author Nikita Gubarkov
*/
public abstract class Q extends P {
	
	
	protected Q() {}
	public static Q create(String s) {
		if(!s.contains("/")) return Z.create(s);
		String[] ss=s.replaceAll("\\(", "").replaceAll("\\)", "").split("/");
		return create(Z.create(ss[0].trim()), N.create(ss[1].trim()));
	}
	public static Q create(Z n, N d) {
		if(d.isZero()) throw new IllegalArgumentException("Denominator cannot be null");
		if(d.isOne()) return n;
		return new QImpl(n, d);
	}
	
	
	
	public boolean isQ() {return true;}
	
	
	public int degree() {
		return 0;
	}
	
	
	public Q getCoefficient(int deg) {
		if(deg==0) return this;
		return N.ZERO;
	}
	
	
	public abstract Z numerator();
	
	
	public abstract N denominator();
	
	
	public Q abs() {
		if(numerator().sign()) return this;
		return create(numerator().abs(), denominator());
	}
	
	
	public boolean sign() {
		return numerator().sign();
	}
	
	
	public boolean isZero() {
		return numerator().isZero();
	}
	
	
	public String toString() {
		return numerator().toString()+"/"+denominator().toString();
	}
	
	
}
