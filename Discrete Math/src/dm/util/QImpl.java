package dm.util;


/**
* @author Nikita Gubarkov
*/
class QImpl extends Q {
	
	
	private final Z numerator;
	private final N denominator;
	
	
	
	protected QImpl(Z n, N d) {
		numerator=n;
		denominator=d;
	}
	
	
	
	public Z numerator() {
		return numerator;
	}
	
	
	public N denominator() {
		return denominator;
	}
	
	
}
