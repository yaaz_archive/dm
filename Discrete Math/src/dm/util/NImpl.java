package dm.util;


/**
* @author Nikita Gubarkov
*/
class NImpl extends N {
	
	
	protected final byte[] digits;
	
	
	
	protected NImpl(int length) {
		digits=new byte[length];
	}
	
	
	
	public byte getDigit(int position) {
		if(position>=digits.length) return 0;
		return digits[position];
	}
	
	
	public int getDigitsNumber() {
		if(digits.length==0) return 1;
		return digits.length;
	}
	
	
}
