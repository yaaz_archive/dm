package dm.util;


/**
* @author Nikita Gubarkov
*/
class PImpl extends P {
	
	
	protected final Q[] coefs;
	
	
	
	protected PImpl(Q[] c) {
		int i=0;
		for(i=c.length-1;i>=0;i--) {
			if(!c[i].isZero()) {
				i++;
				break;
			}
		}
		coefs=new Q[i];
		System.arraycopy(c, 0, coefs, 0, i);
	}
	
	
	
	public int degree() {
		if(coefs.length==0) return 0;
		return coefs.length-1;
	}
	
	
	public Q getCoefficient(int deg) {
		if(deg>=coefs.length) return N.ZERO;
		return coefs[deg];
	}
	
	
}
