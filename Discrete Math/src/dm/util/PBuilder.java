package dm.util;

import java.util.ArrayList;


/**
* @author Nikita Gubarkov
*/
public class PBuilder {
	
	
	private ArrayList<Q> coefs=new ArrayList<>();
	
	
	
	private PBuilder() {}
	public static PBuilder create() {
		return new PBuilder();
	}
	public static PBuilder create(P s) {
		if(s instanceof PImpl) create(((PImpl)s).coefs);
		PBuilder b=new PBuilder();
		for(int i=0;i<=s.degree();i++) b.coefs.add(s.getCoefficient(i));
		return b;
	}
	public static PBuilder create(PBuilder s) {
		PBuilder b=new PBuilder();
		b.coefs.addAll(s.coefs);
		return b;
	}
	public static PBuilder create(Q[] arr) {
		PBuilder b=new PBuilder();
		for(Q q : arr) b.coefs.add(q);
		return b;
	}
	
	
	
	public void setCoefficient(int position, Q coef) {
		while(coefs.size()<=position) coefs.add(N.ZERO);
		coefs.set(position, coef);
	}
	
	
	public Q getCoefficient(int deg) {
		if(deg>=coefs.size()) return N.ZERO;
		return coefs.get(deg);
	}
	
	
	public int degree() {
		for(int i=coefs.size()-1;i>=0;i--) {
			if(!coefs.get(i).isZero()) return i;
		}
		return 0;
	}
	
	
	public boolean isZero() {
		for(int i=0;i<=degree();i++) {
			if(!getCoefficient(i).isZero()) return false;
		}
		return true;
	}
	
	
	public P get() {
		Q[] arr=new Q[degree()+1];
		for(int i=0;i<arr.length;i++) arr[i]=coefs.get(i);
		return P.create(arr);
	}
	
	
	public String toString() {
		return P.toString(this::getCoefficient, degree());
	}
	
	
}
