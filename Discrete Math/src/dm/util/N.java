package dm.util;


/**
* @author Nikita Gubarkov
*/
public abstract class N extends Z {
	
	
	public static final N ZERO=N.create(0);
	public static final N ONE=N.create(1);
	
	
	
	
	
	protected N() {}
	public static N create(long l) {
		if(l<0) throw new IllegalArgumentException("Negative value "+l);
		return create(String.valueOf(l));
	}
	public static N create(String s) {
		NImpl n=new NImpl(s.length());
		for(int i=0;i<n.digits.length;i++) {
			byte b=(byte)((int)s.charAt(n.digits.length-i-1)-48);
			if(b<0||b>9) throw new IllegalArgumentException(b+" digit");
			n.digits[i]=b;
		}
		return n;
	}
	public static N create(byte... arr) {
		int len=0;
		for(int i=arr.length-1;i>=0;i--) {
			if(arr[i]!=0) {
				len=i+1;
				break;
			}
		}
		NImpl n=new NImpl(len);
		System.arraycopy(arr, 0, n.digits, 0, len);
		return n;
	}
	
	
	
	public boolean isN() {return true;}
	
	
	public N abs() {
		return this;
	}
	
	
	public boolean sign() {
		return true;
	}
	
	
	public abstract byte getDigit(int position);
	
	
	public abstract int getDigitsNumber();
	
	
	public boolean isZero() {
		for(int i=0;i<getDigitsNumber();i++) {
			if(getDigit(i)!=0) return false;
		}
		return true;
	}
	public boolean isOne() {
		if(getDigit(0)!=1) return false;
		for(int i=1;i<getDigitsNumber();i++) {
			if(getDigit(i)!=0) return false;
		}
		return true;
	}
	
	
	public String toString() {
		return toString(this::getDigit, getDigitsNumber());
	}
	protected static String toString(DigitGetter arr, int len) {
		if(len==0) return "0";
		StringBuilder s=new StringBuilder();
		for(int i=len-1;i>=0;i--) s.append(arr.get(i));
		return s.toString();
	}
	
	
	
	
	
	@FunctionalInterface
	protected static interface DigitGetter {
		public byte get(int i);
	}
	
	
}
