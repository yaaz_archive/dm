package dm.util;

import java.util.function.IntFunction;


/**
* @author Nikita Gubarkov
*/
public abstract class P {
	
	
	protected P() {}
	public static P create(String s) {
		String[] ss=s.replaceAll("\\s+", "").replaceAll("\\(-", "-(").replaceAll("-", "+-").split("\\+");
		PBuilder b=PBuilder.create();
		for(String t : ss) processPart(b, t);
		if(b.degree()==0) return b.getCoefficient(0);
		return b.get();
	}
	private static void processPart(PBuilder b, String s) {
		if(s.length()==0) return;
		s=s.replaceAll("\\(", "").replaceAll("\\)", "");
		if(!s.contains("x")) {
			b.setCoefficient(0, Q.create(s));
			return;
		}
		if(!s.contains("^")) {
			b.setCoefficient(1, s.equals("x")?N.ONE:(s.equals("-x")?Z.create(N.ONE, false):Q.create(s.replace("x", ""))));
			return;
		}
		String[] ss=s.split("x\\^");
		b.setCoefficient(Integer.valueOf(ss[1]), ss[0].equals("")?N.ONE:(ss[0].equals("-")?Z.create(N.ONE, false):Q.create(ss[0])));
	}
	public static P create(Q... coefs) {
		if(coefs.length==0) return N.ZERO;
		if(coefs.length==1) return coefs[0];
		return new PImpl(coefs);
	}
	
	
	
	public boolean isN() {return false;}
	public boolean isZ() {return false;}
	public boolean isQ() {return false;}
	
	
	public abstract int degree();
	
	
	public abstract Q getCoefficient(int deg);
	
	
	public boolean isZero() {
		for(int i=0;i<=degree();i++) {
			if(!getCoefficient(i).isZero()) return false;
		}
		return true;
	}
	
	
	public String toString() {
		return toString(this::getCoefficient, degree());
	}
	protected static String toString(IntFunction<Q> coef, int degree) {
		StringBuilder s=new StringBuilder();
		boolean first=true;
		for(int i=degree;i>=0;i--) {
			Q c=coef.apply(i);
			if(!c.isZero()) {
				if(!first||!c.sign()) s.append(c.sign()?'+':'-');
				if(!c.isZ()||!((Z)c).abs().isOne()||i==0) {
					boolean brackets=!c.isZ();
					if(brackets) s.append('(');
					s.append(c.abs().toString());
					if(brackets) s.append(')');
				}
				if(i==1) s.append('x');
				else if(i!=0) {
					s.append("x^");
					s.append(i);
				}
				first=false;
			}
		}
		return s.toString();
	}
	
	
}
