package dm.util;

import java.util.Arrays;


/**
* @author Nikita Gubarkov
*/
public class NBuilder {
	
	
	private byte[] digits;
	
	
	
	private NBuilder(int length) {
		digits=new byte[length];
	}
	public static NBuilder create() {
		return new NBuilder(0);
	}
	public static NBuilder create(N s) {
		NBuilder b=NBuilder.create();
		for(int i=0;i<s.getDigitsNumber();i++) b.setDigit(i, s.getDigit(i));
		return b;
	}
	public static NBuilder create(NBuilder s) {
		return create(s.digits);
	}
	public static NBuilder create(byte[] arr) {
		NBuilder n=new NBuilder(arr.length);
		System.arraycopy(arr, 0, n.digits, 0, arr.length);
		return n;
	}
	
	
	
	private void ensureCapacity(int size) {
		if(digits.length<size) {
			int newSize=(digits.length*3)/2;
			if(newSize<size) newSize=size;
			digits=Arrays.copyOf(digits, newSize);
		}
	}
	
	
	
	public void setDigit(int position, int digit) {
		if(digit<0||digit>9) throw new IllegalArgumentException();
		ensureCapacity(position+1);
		digits[position]=(byte) digit;
	}
	
	
	public byte getDigit(int position) {
		if(position>=digits.length) return 0;
		return digits[position];
	}
	
	
	public int getDigitsNumber() {
		for(int i=digits.length-1;i>=0;i--) {
			if(digits[i]!=0) return i+1;
		}
		return 1;
	}
	
	
	public boolean isZero() {
		for(byte b : digits) {
			if(b!=0) return false;
		}
		return true;
	}
	
	
	public N get() {
		return N.create(digits);
	}
	
	
	public String toString() {
		return N.toString(this::getDigit, getDigitsNumber());
	}
	
	
}
