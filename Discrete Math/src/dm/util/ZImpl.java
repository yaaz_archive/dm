package dm.util;


/**
* @author Nikita Gubarkov
*/
class ZImpl extends Z {
	
	
	private final N abs;
	private final boolean sign;
	
	
	
	protected ZImpl(N abs, boolean sign) {
		this.abs=abs;
		this.sign=sign;
	}
	
	
	
	public N abs() {
		return abs;
	}
	
	
	public boolean sign() {
		return sign;
	}
	
	
}
