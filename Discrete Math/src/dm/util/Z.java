package dm.util;


/**
* @author Nikita Gubarkov
*/
public abstract class Z extends Q {
	
	
	protected Z() {}
	public static Z create(long l) {
		return create(String.valueOf(l));
	}
	public static Z create(String s) {
		boolean sn=!s.startsWith("-");
		if(!sn) s=s.substring(1);
		return create(N.create(s), sn);
	}
	public static Z create(N n, boolean sign) {
		return (sign||n.isZero())?n:new ZImpl(n, sign);
	}
	
	
	
	public boolean isZ() {return true;}
	
	
	public Z numerator() {
		return this;
	}
	
	
	public N denominator() {
		return N.ONE;
	}
	
	
	public abstract N abs();
	
	
	public abstract boolean sign();
	
	
	public byte getDigit(int position) {
		return abs().getDigit(position);
	}
	
	
	public int getDigitsNumber() {
		return abs().getDigitsNumber();
	}
	
	
	public boolean isZero() {
		return abs().isZero();
	}
	public boolean isOne() {
		return sign()&&abs().isOne();
	}
	
	
	public long asLong() {
		long res=0;
		long mul=1;
		for(int i=0;i<getDigitsNumber();i++) {
			res+=getDigit(i)*mul;
			mul*=10;
		}
		if(!sign()) res=-res;
		return res;
	}
	public int asInt() {
		return (int) asLong();
	}
	
	
	public String toString() {
		String s=abs().toString();
		if(!sign()) s="-"+s;
		return s;
	}
	
	
}
