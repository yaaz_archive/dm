package dm.util;


/**
* @author Nikita Gubarkov
*/
public class Dk extends N {
	
	
	private final int digit, position;
	private Dk(int d, int p) {
		digit=d;
		position=p;
	}
	public static Dk create(int digit, int power) {
		return new Dk(digit, power);
	}
	
	
	
	public int getDigit() {
		return digit;
	}
	
	
	public byte getDigit(int position) {
		return (byte) (this.position==position?digit:0);
	}
	
	
	public int getDigitsNumber() {
		return position+1;
	}
	
	
	
}
