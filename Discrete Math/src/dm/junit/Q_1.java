//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.Q;
import dm.util.Z;
import dm.util.N;

public class Q_1 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	private static Random random;
	
	@BeforeClass
	public static void prepare(){
		random=new Random();
	}
	
	private String sokr(int chisl, int znam){
		String res = "";
        int a = chisl;
        int b = znam;
        int c = 0;
        if (a==0)
            res="0";
        else {
            if (b>a) {
                c=a;
                a=b;
                b=c;
            }
            while (a%b!=0) {
                c=b;
                b=a%b;
                a=c;
            }
            int mnog = 1;
            if (((chisl/(double)znam)<0)&&((chisl/(double)b)>0))
                mnog=-1;
            res=mnog*chisl/b+((Math.abs(znam/b)!=1)?"/"+String.valueOf(mnog*znam/b):"");
        }
        return res;
	}
	
	int chi=-10;
	
	@Test
	@Repeat(100)
	public void protivipolognue(){
		int chisl=Math.abs(random.nextInt())+1;
		Q test=Q.create(Z.create(-chisl), N.create(chisl));
		String res=sokr(-chisl, chisl);
		assertEquals(res, Procedures.RED_Q_Q(test).toString());
	}
	
	@Test
	@Repeat(100)
	public void ravnue(){
		int chisl=Math.abs(random.nextInt())+1;
		Q test=Q.create(Z.create(chisl), N.create(chisl));
		String res=sokr(chisl, chisl);
		assertEquals(res, Procedures.RED_Q_Q(test).toString());
	}
	
	@Test
	@Repeat(21)
	public void ones(){
		int znam=Math.abs(random.nextInt())+1;
		Q test=Q.create(Z.create(chi), N.create(znam));
		String res=sokr(chi, znam);
		assertEquals(res, Procedures.RED_Q_Q(test).toString());
		chi++;
	}
	
	@Test
	@Repeat(10)
	public void random(){
		int chisl=random.nextInt();
		int znam=Math.abs(random.nextInt())+1;
		Q test=Q.create(Z.create(chisl), N.create(znam));
		String res=sokr(chisl, znam);
		assertEquals(res, Procedures.RED_Q_Q(test).toString());
	}
	
	
}
