//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.Z;


public class Z_3 {

	@Rule
    public RepeatRule repeatRule = new RepeatRule();

	private static Random random;
	
	static int zero;
	
	@BeforeClass
	public static void prepare(){
		random=new Random();
		zero=-10;
	}
	
	@Test
	@Repeat(11)
	public void zeroTest(){
		Z zTest=Z.create(zero);
		int result=-zero;
		assertEquals(String.valueOf(result),String.valueOf(Procedures.MUL_ZM_Z(zTest)));
		zero++;
	}
	
	@Test
	@Repeat(100)
	public void random(){
		long test=random.nextLong();
		Z zTest=Z.create(test);
		long result=-test;
		assertEquals(String.valueOf(result),String.valueOf(Procedures.MUL_ZM_Z(zTest)));
	}
}
