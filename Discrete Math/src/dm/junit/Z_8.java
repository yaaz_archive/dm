//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.Z;

public class Z_8 {

	@Rule
    public RepeatRule repeatRule = new RepeatRule();

	private static Random random;
	
	@BeforeClass
	public static void prepare(){
		random=new Random();
	}
	
	@Test
	public void one1(){
		int first=random.nextInt();
		Z a=Z.create(first);
		Z b=Z.create(1);
		assertEquals(String.valueOf(first), Procedures.MUL_ZZ_Z(a, b).toString());
	}
	
	@Test
	public void one2(){
		int first=random.nextInt();
		Z a=Z.create(1);
		Z b=Z.create(first);
		assertEquals(String.valueOf(first), Procedures.MUL_ZZ_Z(a, b).toString());
	}
	
	@Test
	public void invert(){
		int first=random.nextInt();
		Z a=Z.create(first);
		Z b=Z.create(-1);
		assertEquals(String.valueOf(-first), Procedures.MUL_ZZ_Z(a, b).toString());
	}
	
	@Test
	public void ravnue(){
		long first=random.nextInt()%10000;
		Z a=Z.create(first);
		Z b=Z.create(first);
		assertEquals(String.valueOf(first*first), Procedures.MUL_ZZ_Z(a, b).toString());
	}
	
	@Test
	@Repeat(100)
	public void random(){
		long first=random.nextInt()%10000;
		long second=random.nextInt()%10000;
		Z a=Z.create(first);
		Z b=Z.create(second);
		long result=first*second;
		assertEquals(String.valueOf(result), Procedures.MUL_ZZ_Z(a, b).toString());
	}
	
}
