//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;


public class N_14 {

	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
    private static Random random;
    
    @BeforeClass
    public static void setup() {
	random=new Random();
    }
	
    @Test
    public void singleTest(){
    	int a, b;
    	a=b=random.nextInt(100)+1;
    	N res=Procedures.GCF_NN_N(N.create(a), N.create(b));
    	int mult=a*b;
    	int ans=0;
    	if ((a!=0)&&(b!=0)) {
            while ((a!=0)&&(b!=0)) {
                if (a>b)
                    a=a%b;
                else
                    b=b%a;
            }
            ans=a+b;
        }

    	if (ans!=0)
    		ans=mult/ans;
    	else
    		ans=mult;
    	
    	
    	assertEquals(String.valueOf(ans),res.toString());
    }
    
    @Test
    @Repeat(100)
    public void test(){
    	int a=random.nextInt(100)+1;
    	int b=random.nextInt(100)+1;
    	N res=Procedures.LCM_NN_N(N.create(a), N.create(b));
    	int mult=a*b;
    	int ans=0;
    	if ((a!=0)&&(b!=0)) {
            while ((a!=0)&&(b!=0)) {
                if (a>b)
                    a=a%b;
                else
                    b=b%a;
            }
            ans=a+b;
        }
    	
    	if (ans!=0)
    		ans=mult/ans;
    	else
    		ans=mult;
    	
    	assertEquals(String.valueOf(ans),res.toString());
    }
    
    
}
