//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.Q;
import dm.util.Z;
import dm.util.N;

public class Q_4 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	private static Random random;
	
	@BeforeClass
	public static void prepare(){
		random=new Random();
	}
	
	@Test
	public void zero(){
		Q a=Q.create(Z.create(0), N.create(1));
		assertEquals(String.valueOf(0), Procedures.TRANS_Q_Z(a).toString());
	}
	
	@Test
	public void one(){
		Q a=Q.create(Z.create(1), N.create(1));
		assertEquals(String.valueOf(1), Procedures.TRANS_Q_Z(a).toString());
	}
	
	@Test
	@Repeat(100)
	public void random(){
		int rand=random.nextInt();
		Q a=Q.create(Z.create(rand), N.create(1));
		assertEquals(String.valueOf(rand), Procedures.TRANS_Q_Z(a).toString());
	}
	
}
