//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.Z;


public class Z_6 {

	@Rule
    public RepeatRule repeatRule = new RepeatRule();

	private static Random random;
	
	static int zero, zero2;
	
	@BeforeClass
	public static void prepare(){
		random=new Random();
		zero=-5;
		zero2=-5;
	}
	
	@Test
	@Repeat(11)
	public void zeroTest1(){
		Z zTest=Z.create(zero);
		int rand=random.nextInt();
		Z b=Z.create(rand);
		int result=zero+rand;
		assertEquals(String.valueOf(result),String.valueOf(Procedures.ADD_ZZ_Z(zTest, b)));
		zero++;
	}
	
	@Test
	@Repeat(11)
	public void zeroTest2(){
		Z b=Z.create(zero2);
		int rand=random.nextInt();
		Z zTest=Z.create(rand);
		long result=zero2+rand;
		assertEquals(String.valueOf(result),String.valueOf(Procedures.ADD_ZZ_Z(zTest, b)));
		zero2++;
	}
	
	@Test
	public void protivopolognue(){
		int rand=random.nextInt();
		Z a=Z.create(rand);
		Z b=Z.create(-rand);
		assertEquals(String.valueOf(0),String.valueOf(Procedures.ADD_ZZ_Z(a,b)));
	}
	
	@Test
	@Repeat(100)
	public void random(){
		long testA=random.nextInt();
		long testB=random.nextInt();
		Z a=Z.create(testA);
		Z b=Z.create(testB);
		long result=testA+testB;
		assertEquals(String.valueOf(result),(Procedures.ADD_ZZ_Z(a, b)).toString());
	}
}
