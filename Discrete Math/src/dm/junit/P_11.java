//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import org.junit.*;

import dm.module.Procedures;
import dm.util.P;

public class P_11 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	
	@Test
	public void real(){
		String first="x^2-3x+2";
		String second="x^2-4x+3";
		P a=P.create(first);
		P b=P.create(second);
		
		assertEquals("x-1",Procedures.GCF_PP_P(a, b).toString());
		
	}
	
	
}
