//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;

public class N_7 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	private static int step;
	private static int number;
    private static Random random;
    
    @BeforeClass
    public static void setup() {
	step=-1;
	random=new Random();
    }
    
    @Before
    public void number(){
    	step++;
    	number=random.nextInt(10);
    }
    
    @Test
    @Repeat(101)
    public void checkZeroSum() {
	N a=N.create(step);
	assertEquals(String.valueOf((step*((long)Math.pow(10,number)))), Procedures.MUL_Nk_N(a, number).toString());
    }
	
}
