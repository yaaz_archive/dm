//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;


public class N_5 {
    @Rule
    public RepeatRule repeatRule = new RepeatRule();
    
    private static int number;
    private static Random random;
    
    @BeforeClass
    public static void setup() {
	random=new Random();
	number=random.nextInt(100);
    }
    
    @Test
    public void checkZeroSum() {
	N a=N.create(number);
	N b=N.create(number);
    	assertEquals(String.valueOf(0), Procedures.SUB_NN_N(a,b).toString());
    }
	
    @Test
    public void checkZero() {
	N a=N.create(number);
	N b=N.create(0);
	b=Procedures.SUB_NN_N(a,b);
	assertEquals(String.valueOf(number), b.toString());
    }
	
    @Test
    @Repeat(101)
    public void random() {
	long testLong=Math.abs(random.nextLong());
	long raz=testLong;
	N a=N.create(testLong);
	testLong=Math.abs(random.nextLong());
	raz-=testLong;
	N b=N.create(testLong);
	if(raz<0) {
		raz=-raz;
		N t=a;
		a=b;
		b=t;
	}
	assertEquals(String.valueOf(raz), Procedures.SUB_NN_N(a,b).toString());
    }

}
