package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;


public class N_3 {
	@Rule
	public RepeatRule repeatRule = new RepeatRule();
	
	private static int step;
	private static Random random;
	
	@BeforeClass
	public static void setup() {
		step=0;
		random=new Random();
	}

	@Test
	@Repeat(100)
	public void fromBeginning() {
		N a=N.create(step);
		a=Procedures.ADD_1N_N(a);
		step++;
		assertEquals(String.valueOf(step), a.toString());
	}
	
	@Test
	@Repeat(100)
	public void random() {
		long l=Math.abs(random.nextLong());
		N n=N.create(l);
		assertEquals(String.valueOf(l+1), Procedures.ADD_1N_N(n).toString());
	}

}
