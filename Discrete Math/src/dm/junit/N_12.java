//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;

public class N_12 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	private static int d;
	private static int number;
    private static Random random;
    
    @BeforeClass
    public static void setup() {
	d=-1;
	random=new Random();
    }
    
    @Before
    public void number(){
    	d++;
    	number=random.nextInt(10000)+100000;
    }
    
    @Test
    @Repeat(102)
    public void test(){
    	N a=N.create(number+d);
    	N b=N.create(number);
    	assertEquals(String.valueOf(d),Procedures.MOD_NN_N(a, b).toString());
    }
}
