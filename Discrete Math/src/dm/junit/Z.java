package dm.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Z_1.class, Z_2.class, Z_3.class, Z_4.class, Z_5.class, Z_6.class, Z_7.class, Z_8.class, Z_9.class, Z_10.class  })

public class Z {

}
