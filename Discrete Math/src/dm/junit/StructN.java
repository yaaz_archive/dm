package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;
import dm.junit.RepeatRule.Repeat;
import dm.util.N;
import dm.util.NBuilder;


public class StructN {
	@Rule
	public RepeatRule repeatRule = new RepeatRule();
	
	private static Random random;
	
	@BeforeClass
	public static void setup() {
		random=new Random();
	}

	@Test
	@Repeat(100)
	public void n() {
		long l=Math.abs(random.nextLong());
		N n=N.create(l);
		assertEquals(String.valueOf(l), n.toString());
	}
	
	@Test
	@Repeat(100)
	public void nBuilder() {
		long l=Math.abs(random.nextLong());
		String s=String.valueOf(l);
		NBuilder n=NBuilder.create(N.create(l));
		assertEquals(s, n.toString());
		assertEquals(s, n.get().toString());
	}
	
	@Test
	@Repeat(100)
	public void nBuilderSetDigit() {
		long l=Math.abs(random.nextLong());
		NBuilder n=NBuilder.create();
		String s=String.valueOf(l);
		if(random.nextBoolean()) {
			for(int i=0;i<s.length();i++) n.setDigit(s.length()-i-1, Integer.valueOf(String.valueOf(s.charAt(i))));
		}
		else {
			for(int i=s.length()-1;i>=0;i--) n.setDigit(s.length()-i-1, Integer.valueOf(String.valueOf(s.charAt(i))));
		}
		assertEquals(s, n.toString());
		assertEquals(s, n.get().toString());
	}
	
	
}
