//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import org.junit.*;

import dm.module.Procedures;
import dm.util.P;

public class P_9 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	
	@Test
	public void real(){
		String first="3x^3+2x^2+x";
		String second="x+1";
		P a=P.create(first);
		P b=P.create(second);
		
		assertEquals("3x^2-x+2",Procedures.DIV_PP_P(a, b).toString());
		
	}
	
	
}
