//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.Q;
import dm.util.Z;
import dm.util.N;

public class Q_2 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	private static Random random;
	
	@BeforeClass
	public static void prepare(){
		random=new Random();
	}
	
	int raz=-10;
	
	@Test
	@Repeat(21)
	public void otricqat(){
		int znam=Math.abs(random.nextInt())+1;
		long chisl=-random.nextInt()*znam+raz;
		boolean res=(chisl%znam)==0;
		Q test=Q.create(Z.create(chisl), N.create(znam));
		assertEquals(String.valueOf(res), String.valueOf(Procedures.INT_Q_B(test)));
		raz++;
	}
	
	@Test
	@Repeat(50)
	public void random(){
		int chisl=random.nextInt();
		int znam=Math.abs(random.nextInt())+1;
		boolean res=(chisl%znam)==0;
		Q test=Q.create(Z.create(chisl), N.create(znam));
		assertEquals(String.valueOf(res), String.valueOf(Procedures.INT_Q_B(test)));
	}
	
}
