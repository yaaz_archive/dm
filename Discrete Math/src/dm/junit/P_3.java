//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import org.junit.*;

import dm.module.Procedures;
import dm.util.P;
import dm.util.Q;
import dm.util.Z;
import dm.util.N;

public class P_3 {
	
	@Test
	public void real(){
		String first="5x^3+14x-14/3";
		P a=P.create(first);
		Q q=Q.create(Z.create(-2), N.create(1));
		assertEquals("-10x^3-28x+(28/3)",Procedures.MUL_PQ_P(a, q).toString());
	}
	
	@Test
	public void nol(){
		String first="10x^4+13x^2-5x-2";
		P a=P.create(first);
		Q q=Q.create(Z.create(0), N.create(1));
		assertEquals("0",Procedures.MUL_PQ_P(a, q).toString());
	}
	
	@Test
	public void drob(){
		String first="5x^3+14x-14/3";
		P a=P.create(first);
		Q q=Q.create(Z.create(1), N.create(2));
		assertEquals("(5/2)x^3+7x-(7/3)",Procedures.MUL_PQ_P(a, q).toString());
	}
}
