//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.Z;

public class Z_10 {

	@Rule
    public RepeatRule repeatRule = new RepeatRule();

	private static Random random;
	
	@BeforeClass
	public static void prepare(){
		random=new Random();
	}
	
	@Test
	public void ravnue(){
		int first=random.nextInt(1000)+1000;
		Z a=Z.create(first);
		assertEquals(String.valueOf(0), Procedures.MOD_ZZ_Z(a, a).toString());
	}
	
	@Test
	public void one(){
		int first=random.nextInt(1000)+1000;
		Z a=Z.create(first);
		Z b=Z.create(1);
		assertEquals(String.valueOf(0), Procedures.MOD_ZZ_Z(a, b).toString());
	}
	
	@Test
	@Repeat(100)
	public void random(){
		int first=random.nextInt();
		int second=random.nextInt();
		if(second==0) second=1;
		Z a=Z.create(first);
		Z b=Z.create(second);
		assertEquals(String.valueOf(first%second), Procedures.MOD_ZZ_Z(a, b).toString());
	}
	
}
