//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;


public class N_4 {
    @Rule
    public RepeatRule repeatRule = new RepeatRule();
    
    private static int number;
    private static Random random;
    
    @BeforeClass
    public static void setup() {
	random=new Random();
	number=Math.abs(random.nextInt());
    }
	
    @Test
    public void checkWithZero() {
	N a=N.create(number);
	N b=N.create(0);
	b=Procedures.ADD_NN_N(a,b);
	assertEquals(String.valueOf(number), b.toString());
    }
	
    @Test
    @Repeat(101)
    public void random() {
	long testLong=Math.abs(random.nextInt());
	long sum=testLong;
	N a=N.create(testLong);
	testLong=Math.abs(random.nextInt());
	sum+=testLong;
	N b=N.create(testLong);
	assertEquals(String.valueOf(sum), Procedures.ADD_NN_N(a,b).toString());
    }

}
