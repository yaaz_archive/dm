//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import org.junit.*;

import dm.module.Procedures;
import dm.util.P;


public class P_5 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	
	@Test
	public void real(){
		String first="(5/2)x^3+14x-14/3";
		P a=P.create(first);
		assertEquals("5/2",Procedures.LED_P_Q(a).toString());
	}
	
	@Test
	public void one(){
		String first="x^5+(5/2)x^3+14x-14/3";
		P a=P.create(first);
		assertEquals("1",Procedures.LED_P_Q(a).toString());
	}
	
}
