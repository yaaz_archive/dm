//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import org.junit.*;

import dm.module.Procedures;
import dm.util.P;

public class P_12 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	
	@Test
	public void real(){
		String first="3x^3+2x^2+x";
		P a=P.create(first);
		assertEquals("9x^2+4x+1",Procedures.DER_P_P(a).toString());
		
	}
	
	
}
