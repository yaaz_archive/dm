//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;


public class N_2 {
    @Rule
    public RepeatRule repeatRule = new RepeatRule();
	
    private static int step;
    private static Random random;
	
    @BeforeClass
    public static void setup() {
	step=0;
	random=new Random();
    }
    
    @Test
    @Repeat(11)
    public void fromBeginning() {
	N n=N.create(step);
	assertEquals((step!=0), Procedures.NZER_N_B(n));
	step++;
    }
    
    @Test
    @Repeat(124)
    public void random() {
	long testLong=Math.abs(random.nextLong());
	N n=N.create(testLong);
	assertEquals((testLong!=0), Procedures.NZER_N_B(n));
    }		
}