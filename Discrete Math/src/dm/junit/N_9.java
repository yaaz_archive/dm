//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;

public class N_9 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	private static int d;
	private static int number;
    private static Random random;
    
    @BeforeClass
    public static void setup() {
	random=new Random();
    }
    
    @Before
    public void number(){
    	d=random.nextInt(10);
    	number=random.nextInt(100);
    }
    
    @Test
    @Repeat(123)
    public void test(){
    	N a=N.create(number+10000);
    	N b=N.create(number);
    	N answer=Procedures.SUB_NN_N(a, Procedures.MUL_ND_N(b, d));
    	assertEquals(String.valueOf(number+10000-(number*d)),answer.toString());
    }
	
}
