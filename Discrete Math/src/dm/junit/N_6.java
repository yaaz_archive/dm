//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;

public class N_6 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	private static int step;
    private static Random random;
    
    @BeforeClass
    public static void setup() {
	step=-1;
	random=new Random();
    }
    
    @Before
    public void number(){
    	step++;
    }
    
    @Test
    @Repeat(101)
    public void checkZeroSum() {
    	int n=random.nextInt(10);
	N a=N.create(step);
	assertEquals(String.valueOf((step*n)), Procedures.MUL_ND_N(a, n).toString());
    }
	
    
	
	
}
