//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import org.junit.*;

import dm.module.Procedures;
import dm.util.P;

public class P_1 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	
	@Test
	public void real(){
		String first="5x^3+14x-14/3";
		String second="4x^4-5x^3+x^2+12x+4";
		P a=P.create(first);
		P b=P.create(second);
		assertEquals("4x^4+x^2+26x-(2/3)",Procedures.ADD_PP_P(a, b).toString());
	}
}
