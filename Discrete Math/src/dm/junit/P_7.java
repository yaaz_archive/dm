//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import org.junit.*;

import dm.module.Procedures;
import dm.util.P;

public class P_7 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	
	@Test
	public void real(){
		String first="(5/21)x^3+(3/14)x-15/21";
		P a=P.create(first);
		assertEquals("1/42",Procedures.FAC_P_Q(a).toString());
	}
	
	
}
