package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;
import dm.junit.RepeatRule.Repeat;
import dm.util.Z;


public class StructZ {
	@Rule
	public RepeatRule repeatRule = new RepeatRule();
	
	private static Random random;
	
	@BeforeClass
	public static void setup() {
		random=new Random();
	}

	@Test
	@Repeat(100)
	public void z() {
		long l=random.nextLong();
		Z n=Z.create(l);
		assertEquals(String.valueOf(l), n.toString());
	}
	
	
}
