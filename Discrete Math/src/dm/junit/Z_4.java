//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;


public class Z_4 {

	@Rule
    public RepeatRule repeatRule = new RepeatRule();

	private static Random random;
	
	static int zero;
	
	@BeforeClass
	public static void prepare(){
		random=new Random();
		zero=1;
	}
	
	@Test
	@Repeat(11)
	public void zeroTest(){
		N zTest=N.create(zero);
		assertEquals(String.valueOf(zero),(Procedures.TRANS_N_Z(zTest)).toString());
		zero++;
	}
	
	@Test
	@Repeat(100)
	public void random(){
		long test=Math.abs(random.nextLong());
		N zTest=N.create(test);
		assertEquals(String.valueOf(test),(Procedures.TRANS_N_Z(zTest)).toString());
	}
}
