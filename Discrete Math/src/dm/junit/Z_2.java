//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.Z;


public class Z_2 {

	@Rule
    public RepeatRule repeatRule = new RepeatRule();

	private static Random random;
	
	static int zero;
	
	@BeforeClass
	public static void prepare(){
		random=new Random();
		zero=-10;
	}
	
	@Test
	@Repeat(11)
	public void zeroTest(){
		Z zTest=Z.create(zero);
		int result=(zero>0)?2:(zero<0)?1:0;
		assertEquals(String.valueOf(result),String.valueOf(Procedures.POZ_Z_D(zTest)));
		zero++;
	}
	
	@Test
	@Repeat(100)
	public void random(){
		long test=random.nextLong();
		Z zTest=Z.create(test);
		int result=(test>0)?2:(test<0)?1:0;
		assertEquals(String.valueOf(result),String.valueOf(Procedures.POZ_Z_D(zTest)));
	}
}
