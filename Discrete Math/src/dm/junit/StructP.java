package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;
import dm.junit.RepeatRule.Repeat;
import dm.util.Q;
import dm.util.Z;
import dm.util.N;
import dm.util.P;
import dm.util.PBuilder;


public class StructP {
	@Rule
	public RepeatRule repeatRule = new RepeatRule();
	
	private static Random random;
	
	@BeforeClass
	public static void setup() {
		random=new Random();
	}
	
	@Test
	public void string() {
		P p=P.create(N.ONE, N.ZERO, N.create(265), Z.create(-65), Q.create("-77728/2655"), Q.create("78/25"));
		assertEquals("(78/25)x^5-(77728/2655)x^4-65x^3+265x^2+1", p.toString());
	}

	@Test
	@Repeat(100)
	public void p() {
		int s=Math.abs(random.nextInt())%50;
		PBuilder b=PBuilder.create();
		for(int i=0;i<=s;i++) {
			b.setCoefficient(i, Q.create(Z.create(random.nextLong()), N.create(Math.abs(random.nextLong()))));
		}
		P p=b.get();
		assertEquals(p.toString(), P.create(p.toString()).toString());
	}
	
	
}
