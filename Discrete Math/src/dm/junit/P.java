package dm.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ P_1.class, P_2.class, P_3.class, P_4.class, P_5.class, P_6.class, P_7.class, P_8.class, P_9.class, P_10.class, P_11.class, P_12.class, P_13.class  })

public class P {

}
