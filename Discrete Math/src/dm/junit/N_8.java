//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;

public class N_8 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	private static int step;
	private static int number;
    private static Random random;
    
    @BeforeClass
    public static void setup() {
	step=-1;
	random=new Random();
    }
    
    @Before
    public void number(){
    	step++;
    	number=random.nextInt(100);
    }
    
    @Test
    @Repeat(101)
    public void checkZeroSum() {
	N a=N.create(step);
	N b=N.create(step+number);
	assertEquals(String.valueOf(step*(step+number)), Procedures.MUL_NN_N(a, b).toString());
    }
	
}
