//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import org.junit.*;

import dm.module.Procedures;
import dm.util.P;

public class P_2 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	
	@Test
	public void real(){
		String first="5x^3+14x-14/3";
		String second="4x^4-5x^3+x^2+12x+4";
		P a=P.create(first);
		P b=P.create(second);
		assertEquals("-4x^4+10x^3-x^2+2x-(26/3)",Procedures.SUB_PP_P(a, b).toString());
	}
}
