package dm.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({
	StructN.class, StructZ.class, StructQ.class, StructP.class,
	N.class, Z.class, Q.class, P.class
})
public class Tests {
	
}
