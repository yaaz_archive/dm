//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.Z;
import dm.util.N;
import dm.util.Q;


public class Q_6 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	private static Random random;
	
	@BeforeClass
	public static void prepare(){
		random=new Random();
	}
	
	private long nok(long a, long b) {
		long a1 = a, b1=b;
		long c = 0;
        while (b!=0) {
            a%=b;
            c=a;
            a=b;
            b=c;
        }
        return (a1/a)*b1;
    }
	private String sokr(long chisl, long znam){
		String res = "";
		long a = chisl;
		long b = znam;
		long c = 0;
        if (a==0)
            res="0";
        else {
            if (b>a) {
                c=a;
                a=b;
                b=c;
            }
            while (a%b!=0) {
                c=b;
                b=a%b;
                a=c;
            }
            int mnog = 1;
            if (((chisl/(double)znam)<0)&&((chisl/(double)b)>0))
                mnog=-1;
            res=mnog*chisl/b+((Math.abs(znam/b)!=1)?"/"+String.valueOf(mnog*znam/b):"");
        }
        return res;
	}
	
	
	@Test
	public void protipolog(){
		int chisl1=random.nextInt();
		int znam=Math.abs(random.nextInt())+1;
		Q a=Q.create(Z.create(chisl1), N.create(znam));
		assertEquals("0", Procedures.SUB_QQ_Q(a, a).toString());
	}
	
	@Test
	@Repeat(10)
	public void random(){
		long chisl1=random.nextInt(), chisl2=random.nextInt();
		long znam1=Math.abs(random.nextInt())+1, znam2=Math.abs(random.nextInt())+1;
		Q a=Q.create(Z.create(chisl1), N.create(znam1));
		Q b=Q.create(Z.create(chisl2), N.create(znam2));
		long objZnam=nok(znam1, znam2);
		chisl1*=objZnam/znam1;
		chisl2*=objZnam/znam2;
		String res=sokr(chisl1-chisl2, objZnam);
		assertEquals(res, Procedures.SUB_QQ_Q(a, b).toString());
	}
}
