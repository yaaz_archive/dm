package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;
import dm.junit.RepeatRule.Repeat;
import dm.util.Q;
import dm.util.Z;
import dm.util.N;


public class StructQ {
	@Rule
	public RepeatRule repeatRule = new RepeatRule();
	
	private static Random random;
	
	@BeforeClass
	public static void setup() {
		random=new Random();
	}

	@Test
	@Repeat(100)
	public void q() {
		long a=random.nextLong(), b=Math.abs(random.nextLong());
		if(b==1) b=265;
		Q n=Q.create(Z.create(a), N.create(b));
		assertEquals(String.valueOf(a)+"/"+String.valueOf(b), n.toString());
	}
	
	
}
