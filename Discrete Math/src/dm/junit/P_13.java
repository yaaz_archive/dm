//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import org.junit.*;
import dm.module.Procedures;
import dm.util.P;

public class P_13 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	
	@Test
	public void real(){
		String first="x^8-12x^7+61x^6-172x^5+295x^4-316x^3+207x^2-76x+12";
		P a=P.create(first);
		assertEquals("x^3-6x^2+11x-6",Procedures.NMR_P_P(a).toString());
		
	}
	
	
}
