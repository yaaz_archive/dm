package dm.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ Q_1.class, Q_2.class, Q_3.class, Q_4.class, Q_5.class, Q_6.class, Q_7.class, Q_8.class  })

public class Q {

}
