package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;


public class N_1 {
	@Rule
	public RepeatRule repeatRule = new RepeatRule();
	
	private static int step;
	private static Random random;
	
	@BeforeClass
	public static void setup() {
		step=0;
		random=new Random();
	}

	@Test
	@Repeat(100)
	public void fromBeginning() {
		N a=N.create(step);
		for(int i=Math.max(step-10, 0);i<step+10;i++) {
			N b=N.create(i);
			{
				int res=Procedures.COM_NN_D(a, b);
				int expect=step==i?0:(step>i?2:1);
				assertEquals(expect, res);
			}
			{
				int res=Procedures.COM_NN_D(b, a);
				int expect=i==step?0:(i>step?2:1);
				assertEquals(expect, res);
			}
		}
		step++;
	}
	
	@Test
	@Repeat(1000)
	public void random() {
		long a=Math.abs(random.nextLong()), b=Math.abs(random.nextLong());
		int res=Procedures.COM_NN_D(N.create(a), N.create(b));
		int expect=a==b?0:(a>b?2:1);
		assertEquals(expect, res);
	}

}
