package dm.junit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ N_1.class, N_2.class, N_3.class, N_4.class, N_5.class, N_6.class, N_7.class, N_8.class, N_9.class, N_10.class, N_11.class, N_12.class, N_13.class, N_14.class })
public class N {

}
