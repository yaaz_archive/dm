
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.N;

public class N_10 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	private static int number;
    private static Random random;
    
    @BeforeClass
    public static void setup() {
	random=new Random();
    }
    
    @Before
    public void number(){

    	number=random.nextInt(100)+1;
    }
    
    @Test
    @Repeat(123)
    public void test(){
    	N a=N.create(number+100);
    	N b=N.create(number);
    	N answer=Procedures.DIV_NN_Dk(a, b);
    	int ans=(number+100)/number;
    	int mnog=1;
    	while (mnog*10<ans)
    		mnog*=10;
    	assertEquals(String.valueOf(ans-(ans%mnog)),answer.toString());
    }
	
}
