//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.Z;

public class Q_3 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	private static Random random;
	
	@BeforeClass
	public static void prepare(){
		random=new Random();
	}
	
	int raz=-10;
	
	@Test
	@Repeat(21)
	public void ravnue(){
		Z test=Z.create(raz);
		assertEquals(String.valueOf(raz), String.valueOf(Procedures.TRANS_Z_Q(test)));
		raz++;
	}
	
	@Test
	@Repeat(700)
	public void random(){
		int chisl=random.nextInt();
		Z test=Z.create(chisl);
		assertEquals(String.valueOf(chisl), String.valueOf(Procedures.TRANS_Z_Q(test)));
	}
	
}
