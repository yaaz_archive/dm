//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import java.util.Random;
import org.junit.*;

import dm.junit.RepeatRule.Repeat;
import dm.module.Procedures;
import dm.util.Q;
import dm.util.Z;
import dm.util.N;

public class Q_8 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	private static Random random;
	
	@BeforeClass
	public static void prepare(){
		random=new Random();
	}
	
	private String sokr(long chisl, long znam){
		String res = "";
		long a = chisl;
		long b = znam;
		long c = 0;
        if (a==0)
            res="0";
        else {
            if (b>a) {
                c=a;
                a=b;
                b=c;
            }
            while (a%b!=0) {
                c=b;
                b=a%b;
                a=c;
            }
            int mnog = 1;
            if (((chisl/(double)znam)<0)&&((chisl/(double)b)>0))
                mnog=-1;
            res=mnog*chisl/b+((Math.abs(znam/b)!=1)?"/"+String.valueOf(mnog*znam/b):"");
        }
        return res;
	}
	
	@Test
	public void protovopologn(){
		long chisl1=random.nextInt();
		long znam1=Math.abs(random.nextInt());
		Q a=Q.create(Z.create(chisl1), N.create(znam1));
		Q b=Q.create(Z.create(-chisl1), N.create(znam1));
		String res=sokr(-chisl1*(znam1), znam1*chisl1);
		assertEquals(res, Procedures.DIV_QQ_Q(a, b).toString());
	}
	
	@Test
	public void one(){
		long chisl1=random.nextInt();
		long znam1=Math.abs(random.nextInt());
		Q a=Q.create(Z.create(chisl1), N.create(znam1));
		Q b=Q.create(Z.create(1), N.create(1));
		String res=sokr(chisl1, znam1);
		assertEquals(res, Procedures.DIV_QQ_Q(a, b).toString());
	}
	
	@Test
	public void minusOne(){
		int chisl1=random.nextInt();
		int znam1=Math.abs(random.nextInt());
		Q a=Q.create(Z.create(chisl1), N.create(znam1));
		Q b=Q.create(Z.create(-1), N.create(1));
		String res=sokr(-chisl1, znam1);
		assertEquals(res, Procedures.DIV_QQ_Q(a, b).toString());
	}
	
	@Test
	@Repeat(100)
	public void random(){
		long chisl1=random.nextInt(), chisl2=random.nextInt();
		long znam1=Math.abs(random.nextInt()), znam2=Math.abs(random.nextInt());
		Q a=Q.create(Z.create(chisl1), N.create(znam1));
		Q b=Q.create(Z.create(chisl2), N.create(znam2));
		int mnog=1;
		if (chisl2<0)
			mnog=-1;
		String res=sokr(mnog*chisl1*znam2, znam1*Math.abs(chisl2));
		assertEquals(res, Procedures.DIV_QQ_Q(a, b).toString());
	}
	
}
