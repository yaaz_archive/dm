//Author: Ilin Kirill 6371
package dm.junit;

import static org.junit.Assert.*;
import org.junit.*;

import dm.module.Procedures;
import dm.util.P;

public class P_8 {
	@Rule
    public RepeatRule repeatRule = new RepeatRule();
	
	
	@Test
	public void real(){
		String a="x-2";
		String b="x+2";
		P first=P.create(a);
		P second=P.create(b);
		assertEquals("x^2-4",Procedures.MUL_PP_P(first, second).toString());
		
	}
	
	
}
