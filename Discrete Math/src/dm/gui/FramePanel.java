package dm.gui;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JLayeredPane;


/**
* @author Nikita Gubarkov
*/
class FramePanel extends JLayeredPane {
	private static final long serialVersionUID = 744006421101106395L;
	
	
	
	public static final BufferedImage 	GUI_BG=Util.loadImage("gui_bg"),
										GUI_BG_DEC_A=Util.loadImage("gui_bg_dec_a"),
										GUI_BG_DEC_B=Util.loadImage("gui_bg_dec_b"),
										GUI_BG_DEC_B2=Util.loadImage("gui_bg_dec_b2");
	
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		g.drawImage(GUI_BG, 60, 0, 100, 40, 0, 0, 4, 4, this);
		g.drawImage(GUI_BG, GUI.frame.getWidth()-100, 0, GUI.frame.getWidth()-60, 40, 17, 0, 21, 4, this);
		g.drawImage(GUI_BG, 60, GUI.frame.getHeight()-60, 100, GUI.frame.getHeight()-20, 0, 14, 4, 18, this);
		g.drawImage(GUI_BG, GUI.frame.getWidth()-100, GUI.frame.getHeight()-60, GUI.frame.getWidth()-60, GUI.frame.getHeight()-20, 17, 14, 21, 18, this);
		
		g.drawImage(GUI_BG, 60, 40, 100, GUI.frame.getHeight()-60, 0, 4, 4, 14, this);
		g.drawImage(GUI_BG, GUI.frame.getWidth()-100, 40, GUI.frame.getWidth()-60, GUI.frame.getHeight()-60, 17, 4, 21, 14, this);
		g.drawImage(GUI_BG, 100, 0, GUI.frame.getWidth()-100, 40, 4, 0, 17, 4, this);
		g.drawImage(GUI_BG, 100, GUI.frame.getHeight()-60, GUI.frame.getWidth()-100, GUI.frame.getHeight()-20, 4, 14, 17, 18, this);
		
		g.drawImage(GUI_BG, 100, 40, GUI.frame.getWidth()-100, GUI.frame.getHeight()-60, 4, 4, 17, 14, this);
		
		g.drawImage(GUI_BG_DEC_A, 0, GUI.frame.getHeight()-120, 150, 120, this);
	}
	
	
}