package dm.gui;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicButtonUI;


/**
* @author Nikita Gubarkov
*/
class Button extends JButton {
	private static final long serialVersionUID = 5447581242254543004L;
	
	
	
	public static final BufferedImage 	RUN=Util.loadImage("gui_button_run"),
										RUN_H=Util.loadImage("gui_button_run_h"),
										RUN_P=Util.loadImage("gui_button_run_p"),
										EXIT=Util.loadImage("gui_button_exit"),
										EXIT_H=Util.loadImage("gui_button_exit_h");
	
	
	
	private volatile int state=0;
	private final Runnable onclick;
	
	
	Button(BufferedImage a, BufferedImage b, BufferedImage c, Runnable o) {
		setUI(new UI(a, b, c));
		setBorderPainted(false);
		addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {state=1;}
			public void mouseExited(MouseEvent e) {state=0;}
			public void mousePressed(MouseEvent e) {state=2;onclick.run();}
			public void mouseReleased(MouseEvent e) {state=1;}
		});
		onclick=o;
	}
	
	
	
	
	
	private static class UI extends BasicButtonUI {
				
				
		private final BufferedImage image, imageFocus, imagePress;
		
		
		UI(BufferedImage a, BufferedImage b, BufferedImage c) {
			image=a;
			imageFocus=b;
			imagePress=c;
		}
		
		
		public void update(Graphics g, JComponent c) {
			paint(g, c);
		}
		
		
		public void paint(Graphics g, JComponent c) {
			g.drawImage(((Button)c).state==0?image:(((Button)c).state==1?imageFocus:imagePress), 0, 0, c.getWidth(), c.getHeight(), null);
		}
		
		
	}
	
	
}
