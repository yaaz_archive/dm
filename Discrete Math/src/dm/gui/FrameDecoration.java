package dm.gui;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JComponent;


/**
* @author Nikita Gubarkov
*/
class FrameDecoration extends JComponent {
	private static final long serialVersionUID = -7893792371022249327L;
	
	
	
	private static final Point[] collisionShape=new Point[] {
			new Point(5, 9),
			new Point(1, 10),
			new Point(0, 12),
			new Point(0, 13),
			new Point(1, 14),
			new Point(2, 14),
			new Point(3, 14),
			new Point(3, 12),
			new Point(3, 13),
			new Point(3, 14),
			new Point(2, 14),
			new Point(1, 14),
			new Point(0, 12),
			new Point(0, 11),
			new Point(1, 10),
			new Point(5, 9)
	};
	
	
	
	private volatile boolean feed=false;
	
	
	FrameDecoration() {
		setBounds(GUI.frame.getWidth()-160, GUI.frame.getHeight()-150, 160, 150);
		setCursor(Frame.DEFAULT_CURSOR);
		MouseAdapter ma=new MouseAdapter() {
			public void mouseExited(MouseEvent e) {feed=false;}
			public void mouseMoved(MouseEvent e) {
				int x=e.getX()-80, y=e.getY()-100;
				int d2=x*x+y*y*16;
				feed(d2<1000);
			}
			public void mousePressed(MouseEvent e) {
				Point p=e.getLocationOnScreen();
				GUI.frameListener.mousePressed(new Point(p.x-GUI.frame.getX(), p.y-GUI.frame.getY()));
			}
			public void mouseDragged(MouseEvent e) {
				GUI.frameListener.mouseDragged(e.getLocationOnScreen());
			}
		};
		addMouseListener(ma);
		addMouseMotionListener(ma);
	}
	
	
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(feed?FramePanel.GUI_BG_DEC_B2:FramePanel.GUI_BG_DEC_B, 0, 0, 160, 150, this);
	}
	
	
	public boolean contains(int x, int y) {
		x/=10;y/=10;
		if(x<0||x>=16) return false;
		Point p=collisionShape[x];
		return y>=p.x&&y<=p.y;
	}
	
	
	private void feed(boolean f) {
		if(f!=feed) {
			feed=f;
			setCursor(feed?Frame.FEED_CURSORS[(int) (Math.random()*3)]:Frame.DEFAULT_CURSOR);
			repaint();
		}
	}
	
	
}
