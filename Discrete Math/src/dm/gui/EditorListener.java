package dm.gui;

import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.Method;
import java.util.ArrayList;
import javax.swing.JList;
import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.JTextComponent;

import dm.module.Procedures;


/**
* @author Nikita Gubarkov
*/
class EditorListener implements DocumentListener, KeyListener {
	
	
	private static final String[] variants, variantsSmall;
	static {
		Method[] ms=Procedures.class.getDeclaredMethods();
		variants=new String[ms.length];
		variantsSmall=new String[ms.length];
		for(int i=0;i<ms.length;i++) {
			Method m=ms[i];
			StringBuilder b=new StringBuilder();
			b.append(m.getName());
			b.append('(');
			Class<?>[] ps=m.getParameterTypes();
			for(int j=0;j<ps.length;j++) {
				if(j!=0) b.append(", ");
				b.append(ps[j].getSimpleName());
			}
			b.append(')');
			variants[i]=b.toString();
			variantsSmall[i]=variants[i].toLowerCase();
		}
	}
	
	
	
	
	
	private final JTextComponent field;
	protected JPopupMenu popup;
	private JList<String> list;
	private int processStart, processEnd, variantsNumber;
	private boolean skipListener;
	EditorListener(JTextComponent f) {
		field=f;
		f.getDocument().addDocumentListener(this);
		f.addKeyListener(this);
	}
	
	
	
	public void insertUpdate(DocumentEvent e) {
		try {
			String t=e.getDocument().getText(e.getOffset(), e.getLength()).toLowerCase();
			char c=t.charAt(0);
			if(e.getLength()==1&&isAllowedChar(c)) {
				int start=e.getOffset();
				while(start>0&&isAllowedChar(c)) {
					start--;
					c=e.getDocument().getText(start, 1).toLowerCase().charAt(0);
				}
				if(!isAllowedChar(c)) start++;
				t=e.getDocument().getText(start, e.getOffset()-start+1).toLowerCase();
				processStart=start;
				processEnd=e.getOffset()+1;
				ArrayList<String> vars=new ArrayList<>();
				for(int i=0;i<variants.length;i++) {
					if(variantsSmall[i].startsWith(t)) vars.add(variants[i]);
				}
				if(vars.size()>0) {
					popup=new JPopupMenu();
					variantsNumber=vars.size();
					list=new JList<>(vars.toArray(new String[vars.size()]));
					list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
					list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
					list.setSelectedIndex(0);
					list.addListSelectionListener(new ListSelectionListener() {
						public void valueChanged(ListSelectionEvent e) {
							if(!skipListener) insert();
							else skipListener=false;
						}
					});
					
					popup.add(list);
					
					Rectangle caret=field.modelToView(e.getOffset()+1);
					popup.show(field, caret.x+caret.width, caret.y+caret.height);
					field.requestFocus();
				}
				else if(popup!=null) {
					popup.setVisible(false);
					popup=null;
				}
			}
			else if(popup!=null) {
				popup.setVisible(false);
				popup=null;
			}
		} catch (Exception ex) {ex.printStackTrace();}
	}
	public void removeUpdate(DocumentEvent e) {
		if(popup!=null) {
			popup.setVisible(false);
			popup=null;
		}
	}
	public void changedUpdate(DocumentEvent e) {}
	
	
	
	public void keyTyped(KeyEvent e) {}
	public void keyPressed(KeyEvent e) {
		if(popup!=null) {
			if(e.getKeyCode()==38) {
				int i=list.getSelectedIndex();
				i=Math.max(i-1, 0);
				skipListener=true;
				list.setSelectedIndex(i);
				e.consume();
			}
			else if(e.getKeyCode()==40) {
				int i=list.getSelectedIndex();
				i=Math.min(i+1, variantsNumber-1);
				skipListener=true;
				list.setSelectedIndex(i);
				e.consume();
			}
			else if(e.getKeyCode()==10) {
				insert();
				e.consume();
			}
		}
	}
	public void keyReleased(KeyEvent e) {}
	
	
	
	private void insert() {
		String t=field.getText();
		String a=t.substring(0, processStart), b=t.substring(processEnd);
		a+=list.getSelectedValue();
		field.setText(a+b);
		field.setCaretPosition(a.length());
	}
	
	
	
	private static boolean isAllowedChar(char c) {
		if(Character.isAlphabetic(c)) return true;
		return c=='_'||c=='1';
	}
	
	
}
