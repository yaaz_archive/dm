package dm.gui;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.lang.reflect.Method;
import javax.imageio.ImageIO;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import dm.module.Procedures;


/**
* @author Nikita Gubarkov
*/
class Util {
	
	
	protected static String processCode(String s) {
		int i=-1;
		while((i=s.indexOf('$'))!=-1) {
			int j=s.indexOf('$', i+1);
			if(j==-1) throw new RuntimeException("No closing $");
			s=s.substring(0, i)+"P.create(\""+s.substring(i+1, j)+"\")"+s.substring(j+1);
		}
		return s;
	}
	
	
	
	
	
	private static final ScriptEngine nashorn=new ScriptEngineManager().getEngineByName("nashorn");
	static {
		try {
			nashorn.eval("CORE_PROCEDURES=Java.type('dm.module.Procedures')");
			nashorn.eval("N=Java.type('dm.util.N')");
			nashorn.eval("Z=Java.type('dm.util.Z')");
			nashorn.eval("Q=Java.type('dm.util.Q')");
			nashorn.eval("P=Java.type('dm.util.P')");
			nashorn.eval("print=function(s) {RESULT+=s+'\\n';}");
			Method[] ms=Procedures.class.getDeclaredMethods();
			for(Method m : ms) nashorn.eval(getMethodCode(m));
		} catch (ScriptException e) {
			throw new RuntimeException(e);
		}
	}
	protected static final Object js(String code) {
		try {
			nashorn.eval("RESULT='';");
			nashorn.eval(code);
			return nashorn.eval("RETURN_VALUE=RESULT;");
		} catch (ScriptException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	
	
	
	private static String getMethodCode(Method m) {
		StringBuilder b=new StringBuilder();
		b.append("function ");
		b.append(m.getName());
		b.append('(');
		Class<?>[] ps=m.getParameterTypes();
		for(int i=0;i<ps.length;i++) {
			if(i!=0) b.append(',');
			b.append("arg");
			b.append(i);
		}
		b.append("){");
		for(int i=0;i<ps.length;i++) {
			if(ps[i]==int.class||ps[i]==long.class) {
				b.append("\n\targ");
				b.append(i);
				b.append('=');
				b.append("parseInt(");
				b.append("arg");
				b.append(i);
				b.append(");");
			}
		}
		b.append("\n\treturn CORE_PROCEDURES."+m.getName());
		b.append('(');
		for(int i=0;i<ps.length;i++) {
			if(i!=0) b.append(',');
			b.append("arg");
			b.append(i);
		}
		b.append(");\n}\n");
		return b.toString();
	}
	
	
	
	
	
	protected static BufferedImage loadImage(String name) {
		try {
			return ImageIO.read(Util.class.getResourceAsStream("/dm/assets/"+name+".png"));
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
			return null;
		}
	}
	
	
}
