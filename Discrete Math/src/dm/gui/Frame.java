package dm.gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import javax.swing.JFrame;


/**
* @author Nikita Gubarkov
*/
class Frame extends JFrame {
	private static final long serialVersionUID = -8399848619288435902L;
	
	
	
	public static final Cursor 	DEFAULT_CURSOR=Cursor.getDefaultCursor(),
								RESIZE_HORIZONTAL=Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR),
								RESIZE_VERTICAL=Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR),
								RESIZE_DIAGONAL_A=Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR),
								RESIZE_DIAGONAL_B=Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR),
								TEXT_CURSOR=Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR);
	public static final Cursor[] FEED_CURSORS=new Cursor[3];
	static {
		Toolkit tk=Toolkit.getDefaultToolkit();
		Dimension d=tk.getBestCursorSize(20, 20);
		Point p=new Point(d.width/2, d.height/2);
		for(int i=0;i<FEED_CURSORS.length;i++) FEED_CURSORS[i]=tk.createCustomCursor(Util.loadImage("feed_cursor_"+(i+1)), p, "feed_cursor_"+(i+1));
	}
	
	
	
	
	
	private volatile Cursor currentCursor=DEFAULT_CURSOR;
	
	
	Frame() {
		super("Looooooong math");
		Dimension d=new Dimension(330, 200);
		setMinimumSize(d);
		setResizable(false);
		setUndecorated(true);
		setBackground(new Color(0, true));
		setContentPane(new FramePanel());
		GUI.frameListener=new FrameListener();
		addMouseListener(GUI.frameListener);
		addMouseMotionListener(GUI.frameListener);
		setAlwaysOnTop(true);
	}
	
	
	public boolean isDoubleBuffered() {
		return true;
	}
	
	
	void cursor(Cursor c) {
		if(currentCursor!=c) {
			currentCursor=c;
			setCursor(c);
		}
	}
	
	
}