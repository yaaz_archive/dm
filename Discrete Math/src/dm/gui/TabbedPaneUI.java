package dm.gui;

import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import sun.swing.SwingUtilities2;


/**
* @author Nikita Gubarkov
*/
class TabbedPaneUI extends BasicTabbedPaneUI {
	
	
	private static final BufferedImage	TAB=Util.loadImage("gui_tab"),
										TAB_SELECTED=Util.loadImage("gui_tab_selected");
	
	
	public void paint(Graphics g, JComponent c) {
		int selectedIndex = tabPane.getSelectedIndex();
		int tabPlacement = tabPane.getTabPlacement();
		paintTabArea(g, tabPlacement, selectedIndex);
	}
	
	
	protected void paintTab(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect) {
		Rectangle tabRect = rects[tabIndex];
		int selectedIndex = tabPane.getSelectedIndex();
		boolean isSelected = selectedIndex == tabIndex;
		g.drawImage(isSelected?TAB_SELECTED:TAB, tabRect.x, tabRect.y, tabRect.width, tabRect.height, null);
		String title = tabPane.getTitleAt(tabIndex);
		Font font = tabPane.getFont();
		FontMetrics metrics = SwingUtilities2.getFontMetrics(tabPane, g, font);
		Icon icon = getIconForTab(tabIndex);
		layoutLabel(tabPlacement, metrics, tabIndex, title, icon, tabRect, iconRect, textRect, isSelected);
		if(tabPane.getTabComponentAt(tabIndex) == null) {
			String clippedTitle = title;
			paintText(g, tabPlacement, font, metrics, tabIndex, clippedTitle, textRect, isSelected);
		}
	}
	
	
}