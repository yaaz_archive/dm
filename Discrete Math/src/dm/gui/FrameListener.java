package dm.gui;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


/**
* @author Nikita Gubarkov
*/
class FrameListener extends MouseAdapter {
	
	
	private Point mouseDownCompCoords=null;
	private int fx, fy, fw, fh;
	
	
	public void mouseReleased(MouseEvent e) {mouseDownCompCoords=null;}
	
	
	public void mousePressed(MouseEvent e) {
		mousePressed(e.getPoint());
	}
	public void mousePressed(Point p) {
		mouseDownCompCoords=p;
		fx=GUI.frame.getX();
		fy=GUI.frame.getY();
		fw=GUI.frame.getWidth();
		fh=GUI.frame.getHeight();
	}
	
	
	public void mouseDragged(MouseEvent e) {
		 mouseDragged(e.getLocationOnScreen());
	}
	public void mouseDragged(Point currCoords) {
		switch(GUI.resizeType) {
		case 0:{
			GUI.frame.setLocation(currCoords.x-mouseDownCompCoords.x, currCoords.y-mouseDownCompCoords.y);
		}break;
		case 1:{
			int x=currCoords.x-mouseDownCompCoords.x, y=currCoords.y-mouseDownCompCoords.y;
			int w=fw+fx-x, h=fh+fy-y;
			if(w>=GUI.frame.getMinimumSize().width) {
				if(h>=GUI.frame.getMinimumSize().height) GUI.frame.setBounds(x, y, w, h);
				else GUI.frame.setBounds(x, GUI.frame.getY(), w, GUI.frame.getHeight());
			}
			else if(h>=GUI.frame.getMinimumSize().height) GUI.frame.setBounds(GUI.frame.getX(), y, GUI.frame.getWidth(), h);
		}break;
		case 2:{
			int y=currCoords.y-mouseDownCompCoords.y;
			int h=fh+fy-y;
			if(h>=GUI.frame.getMinimumSize().height) GUI.frame.setBounds(GUI.frame.getX(), y, GUI.frame.getWidth(), h);
		}break;
		case 3:{
			int x=GUI.frame.getX(), y=currCoords.y-mouseDownCompCoords.y;
			int w=currCoords.x-mouseDownCompCoords.x-x+fw, h=fh+fy-y;
			if(h>=GUI.frame.getMinimumSize().height) GUI.frame.setBounds(x, y, w, h);
			else if(w>=GUI.frame.getMinimumSize().width) GUI.frame.setBounds(x, GUI.frame.getY(), w, GUI.frame.getHeight());
		}break;
		case 4:{
			int x=GUI.frame.getX();
			int w=currCoords.x-mouseDownCompCoords.x-x+fw;
			if(w>=GUI.frame.getMinimumSize().width) GUI.frame.setBounds(x, GUI.frame.getY(), w, GUI.frame.getHeight());
		}break;
		case 5:{
			int y=GUI.frame.getY();
			int h=currCoords.y-mouseDownCompCoords.y-y+fh;
			if(h>=GUI.frame.getMinimumSize().height) GUI.frame.setBounds(GUI.frame.getX(), y, GUI.frame.getWidth(), h);
		}break;
		case 6:{
			int x=currCoords.x-mouseDownCompCoords.x, y=GUI.frame.getY();
			int w=fw+fx-x, h=currCoords.y-mouseDownCompCoords.y-y+fh;
			if(w>=GUI.frame.getMinimumSize().width) GUI.frame.setBounds(x, y, w, h);
			else if(h>=GUI.frame.getMinimumSize().height) GUI.frame.setBounds(GUI.frame.getX(), y, GUI.frame.getWidth(), h);
		}break;
		case 7:{
			int x=currCoords.x-mouseDownCompCoords.x;
			int w=fw+fx-x;
			if(w>=GUI.frame.getMinimumSize().width) GUI.frame.setBounds(x, GUI.frame.getY(), w, GUI.frame.getHeight());
		}break;
		}
		if(GUI.resizeType!=0) updateResized();
	}
	
	
	public void updateResized() {
		GUI.panel.setSize(GUI.frame.getWidth()-180, GUI.frame.getHeight()-80);
		GUI.frameDecoration.setLocation(GUI.frame.getWidth()-160, GUI.frame.getHeight()-150);
		GUI.exitButton.setLocation(GUI.frame.getWidth()-120, 30);
	}
	
	
	public void mouseMoved(MouseEvent e) {
		int t=0;
		int x=e.getX(), y=e.getY();
		if(x/10==7&&y/10==1) t=1;
		else if((x-GUI.frame.getWidth())/10==-7&&y/10==1) t=3;
		else if(x/10==7&&(y-GUI.frame.getHeight())/10==-3) t=6;
		else if(x/10>=8&&(x-GUI.frame.getWidth())/10<=-8&&y/10==0) t=2;
		else if((x-GUI.frame.getWidth())/10==-6&&y/10>=2&&(y-GUI.frame.getHeight())/10<=-12) t=4;
		else if(x/10>=8&&(x-GUI.frame.getWidth())/10<=-13&&(y-GUI.frame.getHeight())/10==-2) t=5;
		else if(x/10==6&&y/10>=2&&(y-GUI.frame.getHeight())/10<=-4) t=7;
		GUI.resizeType=t;
		switch(t) {
		case 0:
			GUI.frame.cursor(Frame.DEFAULT_CURSOR);
			break;
		case 1:
			GUI.frame.cursor(Frame.RESIZE_DIAGONAL_A);
			break;
		case 3:
		case 6:
			GUI.frame.cursor(Frame.RESIZE_DIAGONAL_B);
			break;
		case 2:
		case 5:
			GUI.frame.cursor(Frame.RESIZE_VERTICAL);
			break;
		case 4:
		case 7:
			GUI.frame.cursor(Frame.RESIZE_HORIZONTAL);
			break;
		}
	}
	
	
}