package dm.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import dm.Main;


/**
* @author Nikita Gubarkov
*/
public class GUI {
	
	
	private static ExecutorService worker;
	private static FrameMock mock;
	protected static Frame frame;
	protected static FrameListener frameListener;
	protected static JPanel panel;
	protected static FrameDecoration frameDecoration;
	protected static Button exitButton;
	protected static volatile int resizeType=0;
	
	
	
	public static void initMock() {
		mock=new FrameMock();
	}
	public static void init() {
		worker=Executors.newFixedThreadPool(1);
		frame=new Frame();
		{
			panel=new JPanel();
			panel.setCursor(Frame.DEFAULT_CURSOR);
			panel.setBounds(90, 30, GUI.frame.getWidth()-180, GUI.frame.getHeight()-80);
			panel.setLayout(new BorderLayout());
			panel.setBackground(new Color(0xFF99FF));
			frame.getContentPane().add(panel, JLayeredPane.DEFAULT_LAYER);
			frame.getContentPane().add(frameDecoration=new FrameDecoration(), JLayeredPane.POPUP_LAYER);
			{
				exitButton=new Button(Button.EXIT, Button.EXIT_H, Button.EXIT, Main::exit);
				exitButton.setBounds(GUI.frame.getWidth()-120, 30, 20, 20);
				frame.getContentPane().add(exitButton, JLayeredPane.PALETTE_LAYER);
			}
		}
		{
			JTabbedPane tabbedPane=new JTabbedPane();
			tabbedPane.setCursor(Frame.DEFAULT_CURSOR);
			tabbedPane.setUI(new TabbedPaneUI());
			
			{
				JPanel m=new JPanel();
				m.setOpaque(false);
				m.setLayout(new BorderLayout());
				
				JTextArea console=new JTextArea();
				console.setBorder(null);
				console.setOpaque(false);
				console.setCursor(Frame.TEXT_CURSOR);
				console.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
				console.setEditable(false);
				m.add(console, BorderLayout.CENTER);
				
				JPanel c=new JPanel();
				c.setOpaque(false);
				c.setLayout(new BorderLayout());
				c.setBorder(new LineBorder(new Color(0xEE3696), 2));
				JTextField p=new JTextField();
				Runnable run=()->worker.execute(()->{
					try {
						console.setText(Util.js("print("+Util.processCode(p.getText())+");")+console.getText());
					} catch(Exception e) {
						console.setText(e.getMessage()+"\n"+console.getText());
					}
				});
				new EditorListener(p) {
					public void keyPressed(KeyEvent e) {
						if(e.getKeyCode()==10&&popup==null) run.run();
						else super.keyPressed(e);
					}
				};
				p.setBorder(null);
				p.setOpaque(false);
				p.setCursor(Frame.TEXT_CURSOR);
				p.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
				p.setText("ADD_NN_N(ADD_1N_N($64$), $200$)");
				c.add(p, BorderLayout.CENTER);
				Button b=new Button(Button.RUN, Button.RUN_H, Button.RUN_P, run);
				b.setPreferredSize(new Dimension(40, 40));
				c.add(b, BorderLayout.EAST);
				
				m.add(c, BorderLayout.NORTH);
				
				
				tabbedPane.addTab("Функция", m);
			}
			
			{
				JPanel m=new JPanel();
				m.setOpaque(false);
				m.setLayout(new BorderLayout());
				
				JTextArea p=new JTextArea();
				p.setBorder(null);
				p.setOpaque(false);
				p.setCursor(Frame.TEXT_CURSOR);
				p.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
				p.setBorder(new LineBorder(new Color(0xEE3696), 2));
				new EditorListener(p);
				p.setText(
						"var a=$9999999999999999999999999999999999999$;\n"+
						"print(ADD_1N_N(a));\n"+
						"print(MUL_ZM_Z(a));\n"+
						"var b=$-265/12345$;\n"+
						"print(b);\n"+
						"var c=$x^265+(6/9)x^2-x+1$;\n"+
						"var d=$(3/9)x^2-1$;\n"+
						"print(ADD_PP_P(c, d));\n"+
						"print(\"Newfags can't longmath\");"
				);
				m.add(p, BorderLayout.CENTER);
				
				JPanel cc=new JPanel();
				cc.setOpaque(false);
				cc.setLayout(new BorderLayout());
				JPanel c=new JPanel();
				c.setOpaque(false);
				c.setLayout(new BorderLayout());
				JTextArea console=new JTextArea();
				console.setBorder(null);
				console.setOpaque(false);
				console.setCursor(Frame.TEXT_CURSOR);
				console.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
				console.setEditable(false);
				JScrollPane consoleScroll=new JScrollPane(console);
				consoleScroll.setPreferredSize(new Dimension(0, 100));
				consoleScroll.setBorder(null);
				consoleScroll.setBackground(new Color(0xFF99FF));
				consoleScroll.getViewport().setBackground(new Color(0xFF99FF));
				consoleScroll.getVerticalScrollBar().setBackground(new Color(0xFF99FF));
				consoleScroll.getHorizontalScrollBar().setBackground(new Color(0xFF99FF));
				consoleScroll.setAutoscrolls(true);
				c.add(consoleScroll, BorderLayout.CENTER);
				Button b=new Button(Button.RUN, Button.RUN_H, Button.RUN_P, ()->worker.execute(()->{
					String s=null;
					try {
						s=console.getText()+Util.js(Util.processCode(p.getText()));
					} catch(Exception e) {
						s=console.getText()+"\n"+e.getMessage();
					} finally {
						if(s!=null) {
							console.setText(s);
							console.setCaretPosition(s.length());
						}
					}
				}));
				b.setPreferredSize(new Dimension(100, 100));
				c.add(b, BorderLayout.EAST);
				cc.add(c, BorderLayout.CENTER);
				JPanel ct=new JPanel();
				ct.setOpaque(false);
				ct.setPreferredSize(new Dimension(70, 100));
				cc.add(ct, BorderLayout.EAST);
				m.add(cc, BorderLayout.SOUTH);
				
				tabbedPane.addTab("Js", m);
			}
			
			{
				JTextArea m=new JTextArea();
				m.setBorder(null);
				m.setOpaque(false);
				m.setCursor(Frame.DEFAULT_CURSOR);
				m.setFont(new Font(Font.DIALOG, Font.BOLD, 16));
				m.setEditable(false);
				m.setLineWrap(true);
				m.setWrapStyleWord(true);
				m.setText(
						"В этом интерфейсе вы можете опробовать работу системы длинной арифметики\n\n"+
						"1. Вкладка \"Функция\":\nПозволяет проверить работоспособность функций длинной арифметики\n\n"+
						"2. Вкладка \"Js\":\nПозволяет написать скрипт на js с использованием длинной арифметики\n\n"+
						"Используйте $<выражение>$ чтобы задать число или многочлен, например:\n$-2$\n$7/2$\n$x^2+2x+1$\n\n"+
						"СБГЭТУ \"ЛЭТИ\", 6371\n764610@gmail.com"
				);
				tabbedPane.addTab("Справка", m);
			}
			
			panel.add(tabbedPane, BorderLayout.CENTER);
		}
		frame.setLocation(mock.getLocation());
		frame.setVisible(true);
		mock.setVisible(false);
		new Thread(()->{
			final int o=4;
			for(int i=0;i<30;i++) {
				GUI.frame.setBounds(GUI.frame.getX()-o, GUI.frame.getY()-o, GUI.frame.getWidth()+o*2, GUI.frame.getHeight()+o*2);
				GUI.frameListener.updateResized();
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {}
			}
			GUI.frame.setAlwaysOnTop(false);
		}).start();
	}
	
	
}