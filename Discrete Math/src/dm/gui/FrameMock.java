package dm.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.JDialog;


/**
* @author Nikita Gubarkov
*/
class FrameMock extends JDialog {
	private static final long serialVersionUID = -8171872507198144799L;
	
	
	
	FrameMock() {
		Dimension ss=Toolkit.getDefaultToolkit().getScreenSize();
		setMinimumSize(new Dimension(330, 200));
		setLocation(ss.width/2-165, ss.height/2-100);
		setResizable(false);
		setUndecorated(true);
		setBackground(new Color(0, true));
		setContentPane(new Panel());
		setOpacity(0);
		setAlwaysOnTop(true);
		setVisible(true);
		new Thread(()->{
			float o=0;
			while(isVisible()) {
				setOpacity((float) ((Math.sin(o)+1)/2)*0.5f+0.5f);
				o+=0.05f;
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {}
			}
		}).start();
	}
	
	
	
	
	
	private static class Panel extends JComponent {
		private static final long serialVersionUID = 2386493610634168188L;
		public static final BufferedImage MOCK;
		static {
			BufferedImage i=null;
			try {
				i=ImageIO.read(Util.class.getResourceAsStream("/dm/assets/gui_mock.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			MOCK=i;
		}
		
		
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			g.drawImage(MOCK, 0, 0, 330, 200, this);
		}
		
		
	}
	
	
}
